--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: SEQ_DATA_SUPPLY; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_DATA_SUPPLY"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_DATA_SUPPLY" OWNER TO neouser;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: DATA_SUPPLY; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."DATA_SUPPLY" (
    "ID" numeric DEFAULT nextval('public."SEQ_DATA_SUPPLY"'::regclass),
    "CODE" character varying(128),
    "LABEL" character varying(128),
    "ACTIVE" numeric(1,0) NOT NULL
);


ALTER TABLE public."DATA_SUPPLY" OWNER TO neouser;

--
-- Name: SEQ_DEMAND; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_DEMAND"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_DEMAND" OWNER TO neouser;

--
-- Name: DEMAND; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."DEMAND" (
    "ID" numeric DEFAULT nextval('public."SEQ_DEMAND"'::regclass),
    "PERSON_FIELDS_ID" numeric(10,0),
    "STRUCTURE_FIELDS_ID" numeric(10,0),
    "DESCRIPTION" character varying(4096),
    "COMMENT" character varying(2048),
    "STATUT_ID" numeric(10,0),
    "USER_ID" numeric(10,0),
    "OLD_VALUE" character varying(256),
    "NEW_VALUE" character varying(128),
    "POPULATION_ID" numeric(10,0),
    "CREATION_DATE" numeric,
    "RESOLVE_DATE" numeric,
    "STRUCTURE_ID" numeric(10,0),
    "PERSON_ID" numeric(10,0)
);


ALTER TABLE public."DEMAND" OWNER TO neouser;

--
-- Name: SEQ_DEMAND_STATUT; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_DEMAND_STATUT"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_DEMAND_STATUT" OWNER TO neouser;

--
-- Name: DEMAND_STATUT; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."DEMAND_STATUT" (
    "ID" numeric DEFAULT nextval('public."SEQ_DEMAND_STATUT"'::regclass),
    "CODE" character varying(128) NOT NULL,
    "LABEL" character varying(256)
);


ALTER TABLE public."DEMAND_STATUT" OWNER TO neouser;

--
-- Name: SEQ_MAILING_LIST; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_MAILING_LIST"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_MAILING_LIST" OWNER TO neouser;

--
-- Name: MAILING_LIST; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."MAILING_LIST" (
    "ID" numeric DEFAULT nextval('public."SEQ_MAILING_LIST"'::regclass),
    "NAME" character varying(128) NOT NULL,
    "SHARED" numeric(1,0),
    "EXPORTED" numeric(1,0),
    "UPDATE_DATE" numeric,
    "PERSON_ID" numeric(10,0)
);


ALTER TABLE public."MAILING_LIST" OWNER TO neouser;

--
-- Name: SEQ_MAILING_LIST_BAL_CC_RECIPIENTS; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_MAILING_LIST_BAL_CC_RECIPIENTS"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_MAILING_LIST_BAL_CC_RECIPIENTS" OWNER TO neouser;

--
-- Name: MAILING_LIST_BAL_CC_RECIPIENTS; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."MAILING_LIST_BAL_CC_RECIPIENTS" (
    "ID" numeric DEFAULT nextval('public."SEQ_MAILING_LIST_BAL_CC_RECIPIENTS"'::regclass),
    "MAILING_LIST_ID" numeric(10,0) NOT NULL,
    "BAL_CC_RECIPIENTS_ID" numeric(10,0) NOT NULL
);


ALTER TABLE public."MAILING_LIST_BAL_CC_RECIPIENTS" OWNER TO neouser;

--
-- Name: SEQ_MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS" OWNER TO neouser;

--
-- Name: MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS" (
    "ID" numeric DEFAULT nextval('public."SEQ_MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS"'::regclass),
    "MAILING_LIST_ID" numeric(10,0) NOT NULL,
    "BAL_PRINCIPAL_RECIPIENTS_ID" numeric(10,0) NOT NULL
);


ALTER TABLE public."MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS" OWNER TO neouser;

--
-- Name: SEQ_MAILING_LIST_CC_RECIPIENTS; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_MAILING_LIST_CC_RECIPIENTS"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_MAILING_LIST_CC_RECIPIENTS" OWNER TO neouser;

--
-- Name: MAILING_LIST_CC_RECIPIENTS; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."MAILING_LIST_CC_RECIPIENTS" (
    "ID" numeric DEFAULT nextval('public."SEQ_MAILING_LIST_CC_RECIPIENTS"'::regclass),
    "MAILING_LIST_ID" numeric(10,0) NOT NULL,
    "CC_RECIPIENTS_ID" numeric(10,0) NOT NULL
);


ALTER TABLE public."MAILING_LIST_CC_RECIPIENTS" OWNER TO neouser;

--
-- Name: SEQ_MAILING_LIST_PRINCIPAL_RECIPIENTS; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_MAILING_LIST_PRINCIPAL_RECIPIENTS"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_MAILING_LIST_PRINCIPAL_RECIPIENTS" OWNER TO neouser;

--
-- Name: MAILING_LIST_PRINCIPAL_RECIPIENTS; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."MAILING_LIST_PRINCIPAL_RECIPIENTS" (
    "ID" numeric DEFAULT nextval('public."SEQ_MAILING_LIST_PRINCIPAL_RECIPIENTS"'::regclass),
    "MAILING_LIST_ID" numeric(10,0) NOT NULL,
    "PRINCIPAL_RECIPIENTS_ID" numeric(10,0) NOT NULL
);


ALTER TABLE public."MAILING_LIST_PRINCIPAL_RECIPIENTS" OWNER TO neouser;

--
-- Name: S_NEOBASE_CHANGELOG; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."S_NEOBASE_CHANGELOG"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."S_NEOBASE_CHANGELOG" OWNER TO neouser;

--
-- Name: NEOBASE_CHANGELOG; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."NEOBASE_CHANGELOG" (
    "ID" numeric DEFAULT nextval('public."S_NEOBASE_CHANGELOG"'::regclass),
    "CHANGE_ID" character varying(256) NOT NULL,
    "EXECUTION_DATE" numeric(10,0) NOT NULL,
    "HASH" character varying(256) NOT NULL,
    "DESCRIPTION" character varying(2048)
);


ALTER TABLE public."NEOBASE_CHANGELOG" OWNER TO neouser;

--
-- Name: S_NEOBASE_CHANGELOG_LOCK; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."S_NEOBASE_CHANGELOG_LOCK"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."S_NEOBASE_CHANGELOG_LOCK" OWNER TO neouser;

--
-- Name: NEOBASE_CHANGELOG_LOCK; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."NEOBASE_CHANGELOG_LOCK" (
    "ID" numeric DEFAULT nextval('public."S_NEOBASE_CHANGELOG_LOCK"'::regclass),
    "LOCKED" numeric(1,0) NOT NULL
);


ALTER TABLE public."NEOBASE_CHANGELOG_LOCK" OWNER TO neouser;

--
-- Name: S_NEOBASE_SCHEMA; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."S_NEOBASE_SCHEMA"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."S_NEOBASE_SCHEMA" OWNER TO neouser;

--
-- Name: NEOBASE_SCHEMA; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."NEOBASE_SCHEMA" (
    "ID" numeric DEFAULT nextval('public."S_NEOBASE_SCHEMA"'::regclass),
    "SCHEMA" text NOT NULL
);


ALTER TABLE public."NEOBASE_SCHEMA" OWNER TO neouser;

--
-- Name: SEQ_NEO_USER; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_NEO_USER"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_NEO_USER" OWNER TO neouser;

--
-- Name: NEO_USER; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."NEO_USER" (
    "ID" numeric DEFAULT nextval('public."SEQ_NEO_USER"'::regclass),
    "USERNAME" character varying(128),
    "PASSWORD" character varying(128),
    "ACTIVE" numeric(1,0),
    "PASSWORD_CHANGE" numeric(1,0),
    "PROFILE_ID" numeric(10,0),
    "DATA_SUPPLY_ID" numeric(10,0),
    "PERSON_USER_ID" numeric(10,0)
);


ALTER TABLE public."NEO_USER" OWNER TO neouser;

--
-- Name: SEQ_PERSON; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_PERSON"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_PERSON" OWNER TO neouser;

--
-- Name: PERSON; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."PERSON" (
    "ID" numeric DEFAULT nextval('public."SEQ_PERSON"'::regclass),
    "IDENTITY_NUMBER" character varying(1024) NOT NULL,
    "ASP_ID" character varying(128),
    "FIRST_NAME" character varying(128) NOT NULL,
    "LAST_NAME" character varying(128) NOT NULL,
    "FULL_NAME" character varying(256),
    "FUNCTION" character varying(128),
    "MANAGER_ID" numeric(10,0),
    "STRUCTURE_ID" numeric(10,0),
    "MAIL" character varying(1024),
    "UPDATE_DATE" numeric NOT NULL,
    "DATA_SUPPLY_ID" numeric(10,0)
);


ALTER TABLE public."PERSON" OWNER TO neouser;

--
-- Name: SEQ_PERSON_FIELDS; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_PERSON_FIELDS"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_PERSON_FIELDS" OWNER TO neouser;

--
-- Name: PERSON_FIELDS; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."PERSON_FIELDS" (
    "ID" numeric DEFAULT nextval('public."SEQ_PERSON_FIELDS"'::regclass),
    "LABEL" character varying(2048),
    "CODE" character varying(128) NOT NULL,
    "FIELD" character varying(2048)
);


ALTER TABLE public."PERSON_FIELDS" OWNER TO neouser;

--
-- Name: SEQ_PHONE; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_PHONE"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_PHONE" OWNER TO neouser;

--
-- Name: PHONE; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."PHONE" (
    "ID" numeric DEFAULT nextval('public."SEQ_PHONE"'::regclass),
    "NUMBER" character varying(64) NOT NULL,
    "PHONE_CATEGORY_ID" numeric(10,0),
    "STRUCTURE_ID" numeric(10,0),
    "PERSON_ID" numeric(10,0)
);


ALTER TABLE public."PHONE" OWNER TO neouser;

--
-- Name: SEQ_PHONE_CATEGORY; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_PHONE_CATEGORY"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_PHONE_CATEGORY" OWNER TO neouser;

--
-- Name: PHONE_CATEGORY; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."PHONE_CATEGORY" (
    "ID" numeric DEFAULT nextval('public."SEQ_PHONE_CATEGORY"'::regclass),
    "CODE" character varying(32) NOT NULL,
    "LABEL" character varying(1024) NOT NULL,
    "ACTIVE" numeric(1,0) NOT NULL
);


ALTER TABLE public."PHONE_CATEGORY" OWNER TO neouser;

--
-- Name: SEQ_POPULATION; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_POPULATION"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_POPULATION" OWNER TO neouser;

--
-- Name: POPULATION; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."POPULATION" (
    "ID" numeric DEFAULT nextval('public."SEQ_POPULATION"'::regclass),
    "CODE" character varying(1024) NOT NULL,
    "LABEL" character varying(1024),
    "ACTIVE" numeric(1,0)
);


ALTER TABLE public."POPULATION" OWNER TO neouser;

--
-- Name: SEQ_POSTAL_ADDRESS; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_POSTAL_ADDRESS"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_POSTAL_ADDRESS" OWNER TO neouser;

--
-- Name: POSTAL_ADDRESS; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."POSTAL_ADDRESS" (
    "ID" numeric DEFAULT nextval('public."SEQ_POSTAL_ADDRESS"'::regclass),
    "STREET" character varying(128),
    "ZIP_CODE" character varying(128),
    "TOWN" character varying(128),
    "COUNTRY" character varying(128),
    "STRUCTURE_POSTAL_ADDRESS_ID" numeric(10,0),
    "PERSON_POSTAL_ADDRESS_ID" numeric(10,0)
);


ALTER TABLE public."POSTAL_ADDRESS" OWNER TO neouser;

--
-- Name: SEQ_PROFILE; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_PROFILE"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_PROFILE" OWNER TO neouser;

--
-- Name: PROFILE; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."PROFILE" (
    "ID" numeric DEFAULT nextval('public."SEQ_PROFILE"'::regclass),
    "CODE" character varying(256) NOT NULL,
    "LABEL" character varying(256),
    "ACTIVE" numeric(1,0)
);


ALTER TABLE public."PROFILE" OWNER TO neouser;

--
-- Name: SEQ_RATING; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_RATING"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_RATING" OWNER TO neouser;

--
-- Name: RATING; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."RATING" (
    "ID" numeric DEFAULT nextval('public."SEQ_RATING"'::regclass),
    "RATING_VALUE" numeric,
    "DATE" numeric NOT NULL,
    "USER_HASH" character varying(256)
);


ALTER TABLE public."RATING" OWNER TO neouser;

--
-- Name: SEQ_SHARED_MAILBOX; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_SHARED_MAILBOX"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_SHARED_MAILBOX" OWNER TO neouser;

--
-- Name: SEQ_STRUCTURE; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_STRUCTURE"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_STRUCTURE" OWNER TO neouser;

--
-- Name: SEQ_STRUCTURE_FIELDS; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_STRUCTURE_FIELDS"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_STRUCTURE_FIELDS" OWNER TO neouser;

--
-- Name: SEQ_SURVEY_ANSWER; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_SURVEY_ANSWER"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_SURVEY_ANSWER" OWNER TO neouser;

--
-- Name: SEQ_SURVEY_QUESTION; Type: SEQUENCE; Schema: public; Owner: neouser
--

CREATE SEQUENCE IF NOT EXISTS public."SEQ_SURVEY_QUESTION"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEQ_SURVEY_QUESTION" OWNER TO neouser;

--
-- Name: SHARED_MAILBOX; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."SHARED_MAILBOX" (
    "ID" numeric DEFAULT nextval('public."SEQ_SHARED_MAILBOX"'::regclass),
    "NAME" character varying(128),
    "DESCRIPTION" character varying(128),
    "MAIL" character varying(1024) NOT NULL,
    "UPDATE_DATE" numeric NOT NULL,
    "STRUCTURE_ID" numeric(10,0),
    "DATA_SUPPLY_ID" numeric(10,0)
);


ALTER TABLE public."SHARED_MAILBOX" OWNER TO neouser;

--
-- Name: STRUCTURE; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."STRUCTURE" (
    "ID" numeric DEFAULT nextval('public."SEQ_STRUCTURE"'::regclass),
    "ASP_ID" character varying(128),
    "FULL_CODE" character varying(256),
    "CODE" character varying(256),
    "LABEL" character varying(256),
    "PARENT_ID" numeric(10,0),
    "MAIL" character varying(1024),
    "WEB_PORTAL" character varying(256),
    "DATA_SUPPLY_ID" numeric(10,0),
    "UPDATE_DATE" numeric,
    "ASP_SERVICE_ID" numeric(10,0)
);


ALTER TABLE public."STRUCTURE" OWNER TO neouser;

--
-- Name: STRUCTURE_FIELDS; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."STRUCTURE_FIELDS" (
    "ID" numeric DEFAULT nextval('public."SEQ_STRUCTURE_FIELDS"'::regclass),
    "LABEL" character varying(2048),
    "CODE" character varying(128) NOT NULL,
    "FIELD" character varying(2048)
);


ALTER TABLE public."STRUCTURE_FIELDS" OWNER TO neouser;

--
-- Name: SURVEY_ANSWER; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."SURVEY_ANSWER" (
    "ID" numeric DEFAULT nextval('public."SEQ_SURVEY_ANSWER"'::regclass),
    "ANSWER" character varying(4096) NOT NULL,
    "RELATED_QUESTION_ID" numeric(10,0),
    "DATE" numeric
);


ALTER TABLE public."SURVEY_ANSWER" OWNER TO neouser;

--
-- Name: SURVEY_QUESTION; Type: TABLE; Schema: public; Owner: neouser
--

CREATE TABLE IF NOT EXISTS public."SURVEY_QUESTION" (
    "ID" numeric DEFAULT nextval('public."SEQ_SURVEY_QUESTION"'::regclass),
    "CODE" character varying(256) NOT NULL,
    "QUESTION" character varying(1024) NOT NULL,
    "ACTIVE" numeric(1,0) NOT NULL
);


ALTER TABLE public."SURVEY_QUESTION" OWNER TO neouser;


CREATE OR REPLACE FUNCTION public.create_constraint_if_not_exists (
    t_name text, c_name text, constraint_sql text
) 
RETURNS void AS
$$
BEGIN
    -- Look for our constraint
    IF NOT EXISTS (SELECT constraint_name 
                   FROM information_schema.constraint_column_usage 
                   WHERE table_name = t_name  AND constraint_name = c_name) THEN
        EXECUTE constraint_sql;
    END IF;
END;
$$ LANGUAGE 'plpgsql';


--
-- Name: DATA_SUPPLY DATA_SUPPLY_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "DATA_SUPPLY_ID_key" ON public."DATA_SUPPLY" ("ID");


--
-- Name: DEMAND DEMAND_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "DEMAND_ID_key" ON public."DEMAND" ("ID");


--
-- Name: DEMAND_STATUT DEMAND_STATUT_CODE_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "DEMAND_STATUT_CODE_key" ON public."DEMAND_STATUT" ("CODE");


--
-- Name: DEMAND_STATUT DEMAND_STATUT_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "DEMAND_STATUT_ID_key" ON public."DEMAND_STATUT" ("ID");


--
-- Name: MAILING_LIST_BAL_CC_RECIPIENTS MAILING_LIST_BAL_CC_RECIPIENTS_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "MAILING_LIST_BAL_CC_RECIPIENTS_ID_key" ON public."MAILING_LIST_BAL_CC_RECIPIENTS" ("ID");


--
-- Name: MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS_ID_key" ON public."MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS" ("ID");


--
-- Name: MAILING_LIST_CC_RECIPIENTS MAILING_LIST_CC_RECIPIENTS_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "MAILING_LIST_CC_RECIPIENTS_ID_key" ON public."MAILING_LIST_CC_RECIPIENTS" ("ID");


--
-- Name: MAILING_LIST MAILING_LIST_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "MAILING_LIST_ID_key" ON public."MAILING_LIST" ("ID");


--
-- Name: MAILING_LIST_PRINCIPAL_RECIPIENTS MAILING_LIST_PRINCIPAL_RECIPIENTS_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "MAILING_LIST_PRINCIPAL_RECIPIENTS_ID_key" ON public."MAILING_LIST_PRINCIPAL_RECIPIENTS" ("ID");


--
-- Name: NEOBASE_CHANGELOG NEOBASE_CHANGELOG_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "NEOBASE_CHANGELOG_ID_key" ON public."NEOBASE_CHANGELOG" ("ID");


--
-- Name: NEOBASE_CHANGELOG_LOCK NEOBASE_CHANGELOG_LOCK_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "NEOBASE_CHANGELOG_LOCK_ID_key" ON public."NEOBASE_CHANGELOG_LOCK" ("ID");


--
-- Name: NEOBASE_SCHEMA NEOBASE_SCHEMA_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "NEOBASE_SCHEMA_ID_key" ON public."NEOBASE_SCHEMA" ("ID");


--
-- Name: NEO_USER NEO_USER_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "NEO_USER_ID_key" ON public."NEO_USER" ("ID");


--
-- Name: PERSON PERSON_ASP_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PERSON_ASP_ID_key" ON public."PERSON" ("ASP_ID");


--
-- Name: PERSON_FIELDS PERSON_FIELDS_CODE_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PERSON_FIELDS_CODE_key" ON public."PERSON_FIELDS" ("CODE");


--
-- Name: PERSON_FIELDS PERSON_FIELDS_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PERSON_FIELDS_ID_key" ON public."PERSON_FIELDS" ("ID");


--
-- Name: PERSON PERSON_IDENTITY_NUMBER_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PERSON_IDENTITY_NUMBER_key" ON public."PERSON" ("IDENTITY_NUMBER");


--
-- Name: PERSON PERSON_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PERSON_ID_key" ON public."PERSON" ("ID");


--
-- Name: PERSON PERSON_MAIL_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PERSON_MAIL_key" ON public."PERSON" ("MAIL");


--
-- Name: PHONE_CATEGORY PHONE_CATEGORY_CODE_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PHONE_CATEGORY_CODE_key" ON public."PHONE_CATEGORY" ("CODE");


--
-- Name: PHONE_CATEGORY PHONE_CATEGORY_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PHONE_CATEGORY_ID_key" ON public."PHONE_CATEGORY" ("ID");


--
-- Name: PHONE PHONE_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PHONE_ID_key" ON public."PHONE" ("ID");


--
-- Name: POPULATION POPULATION_CODE_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "POPULATION_CODE_key" ON public."POPULATION" ("CODE");


--
-- Name: POPULATION POPULATION_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "POPULATION_ID_key" ON public."POPULATION" ("ID");


--
-- Name: POSTAL_ADDRESS POSTAL_ADDRESS_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "POSTAL_ADDRESS_ID_key" ON public."POSTAL_ADDRESS" ("ID");


--
-- Name: PROFILE PROFILE_CODE_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PROFILE_CODE_key" ON public."PROFILE" ("CODE");


--
-- Name: PROFILE PROFILE_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "PROFILE_ID_key" ON public."PROFILE" ("ID");


--
-- Name: RATING RATING_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "RATING_ID_key" ON public."RATING" ("ID");


--
-- Name: SHARED_MAILBOX SHARED_MAILBOX_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "SHARED_MAILBOX_ID_key" ON public."SHARED_MAILBOX" ("ID");


--
-- Name: SHARED_MAILBOX SHARED_MAILBOX_MAIL_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "SHARED_MAILBOX_MAIL_key" ON public."SHARED_MAILBOX" ("MAIL");


--
-- Name: STRUCTURE STRUCTURE_ASP_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "STRUCTURE_ASP_ID_key" ON public."STRUCTURE" ("ASP_ID");


--
-- Name: STRUCTURE_FIELDS STRUCTURE_FIELDS_CODE_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "STRUCTURE_FIELDS_CODE_key" ON public."STRUCTURE_FIELDS" ("CODE");


--
-- Name: STRUCTURE_FIELDS STRUCTURE_FIELDS_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "STRUCTURE_FIELDS_ID_key" ON public."STRUCTURE_FIELDS" ("ID");


--
-- Name: STRUCTURE STRUCTURE_FULL_CODE_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "STRUCTURE_FULL_CODE_key" ON public."STRUCTURE" ("FULL_CODE");


--
-- Name: STRUCTURE STRUCTURE_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "STRUCTURE_ID_key" ON public."STRUCTURE" ("ID");


--
-- Name: SURVEY_ANSWER SURVEY_ANSWER_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "SURVEY_ANSWER_ID_key" ON public."SURVEY_ANSWER" ("ID");


--
-- Name: SURVEY_QUESTION SURVEY_QUESTION_ID_key; Type: CONSTRAINT; Schema: public; Owner: neouser
--

CREATE UNIQUE INDEX IF NOT EXISTS "SURVEY_QUESTION_ID_key" ON public."SURVEY_QUESTION" ("ID");


--
-- Name: IDX_DATA_SUPPLY_CODE; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_DATA_SUPPLY_CODE" ON public."DATA_SUPPLY" USING btree ("CODE");


--
-- Name: IDX_DATA_SUPPLY_LABEL; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_DATA_SUPPLY_LABEL" ON public."DATA_SUPPLY" USING btree ("LABEL");


--
-- Name: IDX_MAILING_LIST_NAME; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_MAILING_LIST_NAME" ON public."MAILING_LIST" USING btree ("NAME");


--
-- Name: IDX_NEO_USER_USERNAME; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_NEO_USER_USERNAME" ON public."NEO_USER" USING btree ("USERNAME");


--
-- Name: IDX_PERSON_FULL_NAME; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_PERSON_FULL_NAME" ON public."PERSON" USING btree ("FULL_NAME");


--
-- Name: IDX_PERSON_LAST_NAME; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_PERSON_LAST_NAME" ON public."PERSON" USING btree ("LAST_NAME");


--
-- Name: IDX_PHONE_CATEGORY_CODE; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_PHONE_CATEGORY_CODE" ON public."PHONE_CATEGORY" USING btree ("CODE");


--
-- Name: IDX_PHONE_CATEGORY_LABEL; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_PHONE_CATEGORY_LABEL" ON public."PHONE_CATEGORY" USING btree ("LABEL");


--
-- Name: IDX_PROFILE_CODE; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_PROFILE_CODE" ON public."PROFILE" USING btree ("CODE");


--
-- Name: IDX_PROFILE_LABEL; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_PROFILE_LABEL" ON public."PROFILE" USING btree ("LABEL");


--
-- Name: IDX_SHARED_MAILBOX_NAME; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_SHARED_MAILBOX_NAME" ON public."SHARED_MAILBOX" USING btree ("NAME");


--
-- Name: IDX_STRUCTURE_ASP_ID; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_STRUCTURE_ASP_ID" ON public."STRUCTURE" USING btree ("ASP_ID");


--
-- Name: IDX_STRUCTURE_CODE; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_STRUCTURE_CODE" ON public."STRUCTURE" USING btree ("CODE");


--
-- Name: IDX_STRUCTURE_FULL_CODE; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_STRUCTURE_FULL_CODE" ON public."STRUCTURE" USING btree ("FULL_CODE");


--
-- Name: IDX_STRUCTURE_LABEL; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_STRUCTURE_LABEL" ON public."STRUCTURE" USING btree ("LABEL");


--
-- Name: IDX_STRUCTURE_PARENT_ID; Type: INDEX; Schema: public; Owner: neouser
--

CREATE INDEX IF NOT EXISTS "IDX_STRUCTURE_PARENT_ID" ON public."STRUCTURE" USING btree ("PARENT_ID");


--
-- Name: DEMAND DEMAND_PERSON_FIELDS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON_FIELDS', 'DEMAND_PERSON_FIELDS_ID_fkey', 'ALTER TABLE ONLY public."DEMAND" ADD CONSTRAINT "DEMAND_PERSON_FIELDS_ID_fkey" FOREIGN KEY ("PERSON_FIELDS_ID") REFERENCES public."PERSON_FIELDS"("ID")');


--
-- Name: DEMAND DEMAND_PERSON_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'DEMAND_PERSON_ID_fkey', 'ALTER TABLE ONLY public."DEMAND" ADD CONSTRAINT "DEMAND_PERSON_ID_fkey" FOREIGN KEY ("PERSON_ID") REFERENCES public."PERSON"("ID") ON DELETE CASCADE');


--
-- Name: DEMAND DEMAND_POPULATION_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('POPULATION', 'DEMAND_POPULATION_ID_fkey', 'ALTER TABLE ONLY public."DEMAND" ADD CONSTRAINT "DEMAND_POPULATION_ID_fkey" FOREIGN KEY ("POPULATION_ID") REFERENCES public."POPULATION"("ID")');


--
-- Name: DEMAND DEMAND_STATUT_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('DEMAND_STATUT', 'DEMAND_STATUT_ID_fkey', 'ALTER TABLE ONLY public."DEMAND" ADD CONSTRAINT "DEMAND_STATUT_ID_fkey" FOREIGN KEY ("STATUT_ID") REFERENCES public."DEMAND_STATUT"("ID")');


--
-- Name: DEMAND DEMAND_STRUCTURE_FIELDS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE_FIELDS', 'DEMAND_STRUCTURE_FIELDS_ID_fkey', 'ALTER TABLE ONLY public."DEMAND" ADD CONSTRAINT "DEMAND_STRUCTURE_FIELDS_ID_fkey" FOREIGN KEY ("STRUCTURE_FIELDS_ID") REFERENCES public."STRUCTURE_FIELDS"("ID")');


--
-- Name: DEMAND DEMAND_STRUCTURE_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE', 'DEMAND_STRUCTURE_ID_fkey', 'ALTER TABLE ONLY public."DEMAND" ADD CONSTRAINT "DEMAND_STRUCTURE_ID_fkey" FOREIGN KEY ("STRUCTURE_ID") REFERENCES public."STRUCTURE"("ID") ON DELETE CASCADE');


--
-- Name: DEMAND DEMAND_USER_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('NEO_USER', 'DEMAND_USER_ID_fkey', 'ALTER TABLE ONLY public."DEMAND" ADD CONSTRAINT "DEMAND_USER_ID_fkey" FOREIGN KEY ("USER_ID") REFERENCES public."NEO_USER"("ID")');


--
-- Name: MAILING_LIST_BAL_CC_RECIPIENTS MAILING_LIST_BAL_CC_RECIPIENTS_BAL_CC_RECIPIENTS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('SHARED_MAILBOX', 'MAILING_LIST_BAL_CC_RECIPIENTS_BAL_CC_RECIPIENTS_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_BAL_CC_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_BAL_CC_RECIPIENTS_BAL_CC_RECIPIENTS_ID_fkey" FOREIGN KEY ("BAL_CC_RECIPIENTS_ID") REFERENCES public."SHARED_MAILBOX"("ID") ON DELETE CASCADE');


--
-- Name: MAILING_LIST_BAL_CC_RECIPIENTS MAILING_LIST_BAL_CC_RECIPIENTS_MAILING_LIST_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('MAILING_LIST', 'MAILING_LIST_BAL_CC_RECIPIENTS_MAILING_LIST_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_BAL_CC_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_BAL_CC_RECIPIENTS_MAILING_LIST_ID_fkey" FOREIGN KEY ("MAILING_LIST_ID") REFERENCES public."MAILING_LIST"("ID") ON DELETE CASCADE');


--
-- Name: MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS_MAILING_LIST_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('MAILING_LIST', 'MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS_MAILING_LIST_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS_MAILING_LIST_ID_fkey" FOREIGN KEY ("MAILING_LIST_ID") REFERENCES public."MAILING_LIST"("ID") ON DELETE CASCADE');


--
-- Name: MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS MAILING_LIST_BAL_PRINCIPAL_REC_BAL_PRINCIPAL_RECIPIENTS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('SHARED_MAILBOX', 'MAILING_LIST_BAL_PRINCIPAL_REC_BAL_PRINCIPAL_RECIPIENTS_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_BAL_PRINCIPAL_REC_BAL_PRINCIPAL_RECIPIENTS_ID_fkey" FOREIGN KEY ("BAL_PRINCIPAL_RECIPIENTS_ID") REFERENCES public."SHARED_MAILBOX"("ID") ON DELETE CASCADE');


--
-- Name: MAILING_LIST_CC_RECIPIENTS MAILING_LIST_CC_RECIPIENTS_CC_RECIPIENTS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'MAILING_LIST_CC_RECIPIENTS_CC_RECIPIENTS_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_CC_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_CC_RECIPIENTS_CC_RECIPIENTS_ID_fkey" FOREIGN KEY ("CC_RECIPIENTS_ID") REFERENCES public."PERSON"("ID") ON DELETE CASCADE');


--
-- Name: MAILING_LIST_CC_RECIPIENTS MAILING_LIST_CC_RECIPIENTS_MAILING_LIST_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('MAILING_LIST', 'MAILING_LIST_CC_RECIPIENTS_MAILING_LIST_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_CC_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_CC_RECIPIENTS_MAILING_LIST_ID_fkey" FOREIGN KEY ("MAILING_LIST_ID") REFERENCES public."MAILING_LIST"("ID") ON DELETE CASCADE');


--
-- Name: MAILING_LIST MAILING_LIST_PERSON_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'MAILING_LIST_PERSON_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST" ADD CONSTRAINT "MAILING_LIST_PERSON_ID_fkey" FOREIGN KEY ("PERSON_ID") REFERENCES public."PERSON"("ID")');


--
-- Name: MAILING_LIST_PRINCIPAL_RECIPIENTS MAILING_LIST_PRINCIPAL_RECIPIENTS_MAILING_LIST_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('MAILING_LIST', 'MAILING_LIST_PRINCIPAL_RECIPIENTS_MAILING_LIST_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_PRINCIPAL_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_PRINCIPAL_RECIPIENTS_MAILING_LIST_ID_fkey" FOREIGN KEY ("MAILING_LIST_ID") REFERENCES public."MAILING_LIST"("ID") ON DELETE CASCADE');


--
-- Name: MAILING_LIST_PRINCIPAL_RECIPIENTS MAILING_LIST_PRINCIPAL_RECIPIENTS_PRINCIPAL_RECIPIENTS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'MAILING_LIST_PRINCIPAL_RECIPIENTS_PRINCIPAL_RECIPIENTS_ID_fkey', 'ALTER TABLE ONLY public."MAILING_LIST_PRINCIPAL_RECIPIENTS" ADD CONSTRAINT "MAILING_LIST_PRINCIPAL_RECIPIENTS_PRINCIPAL_RECIPIENTS_ID_fkey" FOREIGN KEY ("PRINCIPAL_RECIPIENTS_ID") REFERENCES public."PERSON"("ID") ON DELETE CASCADE');


--
-- Name: NEO_USER NEO_USER_DATA_SUPPLY_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('DATA_SUPPLY', 'NEO_USER_DATA_SUPPLY_ID_fkey', 'ALTER TABLE ONLY public."NEO_USER" ADD CONSTRAINT "NEO_USER_DATA_SUPPLY_ID_fkey" FOREIGN KEY ("DATA_SUPPLY_ID") REFERENCES public."DATA_SUPPLY"("ID")');


--
-- Name: NEO_USER NEO_USER_PERSON_USER_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'NEO_USER_PERSON_USER_ID_fkey', 'ALTER TABLE ONLY public."NEO_USER" ADD CONSTRAINT "NEO_USER_PERSON_USER_ID_fkey" FOREIGN KEY ("PERSON_USER_ID") REFERENCES public."PERSON"("ID")');


--
-- Name: NEO_USER NEO_USER_PROFILE_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PROFILE', 'NEO_USER_PROFILE_ID_fkey', 'ALTER TABLE ONLY public."NEO_USER" ADD CONSTRAINT "NEO_USER_PROFILE_ID_fkey" FOREIGN KEY ("PROFILE_ID") REFERENCES public."PROFILE"("ID")');


--
-- Name: PERSON PERSON_DATA_SUPPLY_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('DATA_SUPPLY', 'PERSON_DATA_SUPPLY_ID_fkey', 'ALTER TABLE ONLY public."PERSON" ADD CONSTRAINT "PERSON_DATA_SUPPLY_ID_fkey" FOREIGN KEY ("DATA_SUPPLY_ID") REFERENCES public."DATA_SUPPLY"("ID")');


--
-- Name: PERSON PERSON_MANAGER_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'PERSON_MANAGER_ID_fkey', 'ALTER TABLE ONLY public."PERSON" ADD CONSTRAINT "PERSON_MANAGER_ID_fkey" FOREIGN KEY ("MANAGER_ID") REFERENCES public."PERSON"("ID")');


--
-- Name: PERSON PERSON_STRUCTURE_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE', 'PERSON_STRUCTURE_ID_fkey', 'ALTER TABLE ONLY public."PERSON" ADD CONSTRAINT "PERSON_STRUCTURE_ID_fkey" FOREIGN KEY ("STRUCTURE_ID") REFERENCES public."STRUCTURE"("ID")');


--
-- Name: PHONE PHONE_PERSON_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'PHONE_PERSON_ID_fkey', 'ALTER TABLE ONLY public."PHONE" ADD CONSTRAINT "PHONE_PERSON_ID_fkey" FOREIGN KEY ("PERSON_ID") REFERENCES public."PERSON"("ID") ON DELETE CASCADE');


--
-- Name: PHONE PHONE_PHONE_CATEGORY_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PHONE_CATEGORY', 'PHONE_PHONE_CATEGORY_ID_fkey', 'ALTER TABLE ONLY public."PHONE" ADD CONSTRAINT "PHONE_PHONE_CATEGORY_ID_fkey" FOREIGN KEY ("PHONE_CATEGORY_ID") REFERENCES public."PHONE_CATEGORY"("ID")');


--
-- Name: PHONE PHONE_STRUCTURE_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE', 'PHONE_STRUCTURE_ID_fkey', 'ALTER TABLE ONLY public."PHONE" ADD CONSTRAINT "PHONE_STRUCTURE_ID_fkey" FOREIGN KEY ("STRUCTURE_ID") REFERENCES public."STRUCTURE"("ID") ON DELETE CASCADE');


--
-- Name: POSTAL_ADDRESS POSTAL_ADDRESS_PERSON_POSTAL_ADDRESS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('PERSON', 'POSTAL_ADDRESS_PERSON_POSTAL_ADDRESS_ID_fkey', 'ALTER TABLE ONLY public."POSTAL_ADDRESS" ADD CONSTRAINT "POSTAL_ADDRESS_PERSON_POSTAL_ADDRESS_ID_fkey" FOREIGN KEY ("PERSON_POSTAL_ADDRESS_ID") REFERENCES public."PERSON"("ID") ON DELETE CASCADE');


--
-- Name: POSTAL_ADDRESS POSTAL_ADDRESS_STRUCTURE_POSTAL_ADDRESS_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE', 'POSTAL_ADDRESS_STRUCTURE_POSTAL_ADDRESS_ID_fkey', 'ALTER TABLE ONLY public."POSTAL_ADDRESS" ADD CONSTRAINT "POSTAL_ADDRESS_STRUCTURE_POSTAL_ADDRESS_ID_fkey" FOREIGN KEY ("STRUCTURE_POSTAL_ADDRESS_ID") REFERENCES public."STRUCTURE"("ID") ON DELETE CASCADE');


--
-- Name: SHARED_MAILBOX SHARED_MAILBOX_DATA_SUPPLY_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('DATA_SUPPLY', 'SHARED_MAILBOX_DATA_SUPPLY_ID_fkey', 'ALTER TABLE ONLY public."SHARED_MAILBOX" ADD CONSTRAINT "SHARED_MAILBOX_DATA_SUPPLY_ID_fkey" FOREIGN KEY ("DATA_SUPPLY_ID") REFERENCES public."DATA_SUPPLY"("ID")');


--
-- Name: SHARED_MAILBOX SHARED_MAILBOX_STRUCTURE_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE', 'SHARED_MAILBOX_STRUCTURE_ID_fkey', 'ALTER TABLE ONLY public."SHARED_MAILBOX" ADD CONSTRAINT "SHARED_MAILBOX_STRUCTURE_ID_fkey" FOREIGN KEY ("STRUCTURE_ID") REFERENCES public."STRUCTURE"("ID")');


--
-- Name: STRUCTURE STRUCTURE_ASP_SERVICE_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE', 'STRUCTURE_ASP_SERVICE_ID_fkey', 'ALTER TABLE ONLY public."STRUCTURE" ADD CONSTRAINT "STRUCTURE_ASP_SERVICE_ID_fkey" FOREIGN KEY ("ASP_SERVICE_ID") REFERENCES public."STRUCTURE"("ID")');


--
-- Name: STRUCTURE STRUCTURE_DATA_SUPPLY_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('DATA_SUPPLY', 'STRUCTURE_DATA_SUPPLY_ID_fkey', 'ALTER TABLE ONLY public."STRUCTURE" ADD CONSTRAINT "STRUCTURE_DATA_SUPPLY_ID_fkey" FOREIGN KEY ("DATA_SUPPLY_ID") REFERENCES public."DATA_SUPPLY"("ID")');


--
-- Name: STRUCTURE STRUCTURE_PARENT_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('STRUCTURE', 'STRUCTURE_PARENT_ID_fkey', 'ALTER TABLE ONLY public."STRUCTURE" ADD CONSTRAINT "STRUCTURE_PARENT_ID_fkey" FOREIGN KEY ("PARENT_ID") REFERENCES public."STRUCTURE"("ID")');


--
-- Name: SURVEY_ANSWER SURVEY_ANSWER_RELATED_QUESTION_ID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: neouser
--

SELECT public.create_constraint_if_not_exists('SURVEY_QUESTION', 'SURVEY_ANSWER_RELATED_QUESTION_ID_fkey', 'ALTER TABLE ONLY public."SURVEY_ANSWER" ADD CONSTRAINT "SURVEY_ANSWER_RELATED_QUESTION_ID_fkey" FOREIGN KEY ("RELATED_QUESTION_ID") REFERENCES public."SURVEY_QUESTION"("ID")');


--
-- PostgreSQL database dump complete
--
