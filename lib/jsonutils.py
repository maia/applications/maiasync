"""Custom utils to handle JSON data"""

import logging
import json

log = logging.getLogger()


def iter_json_objects(filename, data_supply=None):
    with open(filename, "r") as fin:
        j = json.load(fin)
    for item in j:
        yield item
