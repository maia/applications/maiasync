"""Class representing the LDAP connector"""

import logging
import sys
import warnings
import ldap  # type: ignore
from ldap import modlist

from lib.ldifutils import LDIFNeedsCorrection, get_ignore_attr_types

log = logging.getLogger()


class LDAPConnector:
    def __init__(self, host, port, binddn, password, base, dry_run=False):
        self.dry_run = dry_run
        if not dry_run:
            self.conn = ldap.initialize(f"ldap://{host}:{port}")
            try:
                self.conn.protocol_version = ldap.VERSION3
                self.conn.simple_bind_s(binddn, password)
            except ldap.SERVER_DOWN as e:
                log.critical(e.args[0]["desc"])
                sys.exit(1)
            except ldap.INVALID_CREDENTIALS as e:
                log.critical(e.args[0]["desc"])
                sys.exit(1)
            except ldap.LDAPError:
                log.critical(exc_info=True)
                sys.exit(1)

        self.base = base

        self.handled_suffixes = [
            f"ou={data_supply},ou=gouv,{base}".lower()
            for data_supply in (
                "MA",
                "MC",
                "MTECT",
                "ALCD",
                "MIOM",
                "MEAE",
                "MINARM",
                "PM",
                "MEFSIN",
                "MASA",
                "MENJ",
                "MJ",
                "MTPEI",
            )
        ]
        self.stats = {
            suffix: {"read": 0, "add": 0, "modify": 0, "delete": 0}
            for suffix in self.handled_suffixes
        }
        self.cache = {suffix: None for suffix in self.handled_suffixes}
        self.remainder = {suffix: set() for suffix in self.handled_suffixes}

        # This suffix is separate as it is a parent of handled ones
        # Therefore, we can't initialize it like the rest, or we will get duplicates
        self.stats[self.base] = {"read": 0, "add": 0, "modify": 0, "delete": 0}
        self.cache[self.base] = {}
        self.remainder[self.base] = set()

    def is_bootstrapped(self):
        """Returns a boolean, wether self.base exists in LDAP or not"""

        if self.dry_run:
            return True
        try:
            self.conn.search_s(base=self.base, scope=ldap.SCOPE_BASE)
        except ldap.NO_SUCH_OBJECT:
            return False
        return True

    def search(self, data_supply, base=None):
        """Perform an LDAP SEARCH <data_supply> filter, and yield results"""

        if base is None:
            base = self.base

        if self.dry_run:
            log.debug("-- DRY-RUN -- SEARCH %s in %s", data_supply, base)
            return None
        for entry in self.conn.search_s(
            base=base,
            scope=ldap.SCOPE_SUBTREE,
            filterstr=f"(MaiaDataSupply={data_supply})",
        ):
            yield entry

    def add(self, dn, entry, ignore_attr_types):
        """Perform an LDAP ADD operation"""

        ldif = modlist.addModlist(
            entry,
            ignore_attr_types=ignore_attr_types,
        )

        if self.dry_run:
            log.debug("-- DRY-RUN -- ADD %s", dn)
            return

        try:
            self.conn.add_s(dn, ldif)
        except ldap.NO_SUCH_OBJECT:
            self.create_missing_intermediary_entries(dn)
            self.add(dn, entry, ignore_attr_types)
        except ldap.INVALID_DN_SYNTAX as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.INVALID_SYNTAX as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.NAMING_VIOLATION as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.UNDEFINED_TYPE as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.OBJECT_CLASS_VIOLATION as e:
            raise NotImplementedError(
                f"Unhandled Object class violation for {entry['objectclass']}: "
                f"{e.args[0]['info']}"
            ) from e

        self.update_cache(dn, entry)
        base = self.find_base_from_dn(dn)
        self.stats[base]["add"] += 1
        log.debug("Added new entry %s", dn)

    def modify(self, dn, entry, ignore_attr_types):
        """Perform an LDAP MOD operation"""

        old_entry = self.get_entry(dn)  # search for the existing entry
        ldif = modlist.modifyModlist(
            old_entry,
            entry,
            ignore_attr_types=ignore_attr_types,
        )

        if self.dry_run:
            log.debug("-- DRY-RUN -- MOD %s", dn)
            return

        try:
            self.conn.modify_s(dn, ldif)
        except ldap.INVALID_DN_SYNTAX as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.INVALID_SYNTAX as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.NAMING_VIOLATION as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.UNDEFINED_TYPE as e:
            error_msg = f"{e.args[0]['desc']}: {e.args[0]['info']}\n  dn: {dn}"
            warnings.warn(error_msg, LDIFNeedsCorrection)
        except ldap.OBJECT_CLASS_VIOLATION as e:
            raise NotImplementedError(
                f"Unhandled class violation for {entry['objectclass']}: "
                f"{e.args[0]['info']}"
            ) from e

        self.update_cache(dn, entry)
        base = self.find_base_from_dn(dn)
        self.stats[base]["modify"] += 1
        log.debug("Updated existing entry %s", dn)

    def delete(self, dn):
        """Perform an LDAP DEL operation, return wether DN was deleted"""

        if self.dry_run:
            log.debug("-- DRY-RUN -- DEL %s", dn)
            return False

        try:
            self.conn.delete_s(dn)
        except ldap.NO_SUCH_OBJECT:
            return False
        except ldap.NOT_ALLOWED_ON_NONLEAF:
            log.debug("Not deleting non leaf %s", dn)
            return False

        return True

    def create_missing_intermediary_entries(self, dn):
        """Called when NO_SUCH_OBJECT, meaning we're missing some of the dn's parents"""

        log.info("Creating missing intermediary entries for %s", dn)
        for intermediary_dn in self._iter_missing_intermediary_entries(dn):
            if intermediary_dn.lower().startswith("ou"):
                entry = {
                    "objectclass": [
                        b"organizationalUnit",
                        b"frGovOrganizationalUnit",
                        b"Top",
                    ],
                    "ou": [intermediary_dn.split(",")[0].split("=")[1].encode()],
                }
            elif intermediary_dn.lower().startswith("dc"):
                entry = {
                    "objectclass": [b"domain", b"top"],
                    "dc": [intermediary_dn.split(",")[0].split("=")[1].encode()],
                }
            elif intermediary_dn.lower().startswith("dmdname"):
                entry = {
                    "objectclass": [b"dmd"],
                    "dmdName": [intermediary_dn.split(",")[0].split("=")[1].encode()],
                }
            else:
                raise NotImplementedError(
                    f"Unhandled intermediary DN: {intermediary_dn}"
                )
            try:
                self.add(intermediary_dn, entry, [])
            except ldap.ALREADY_EXISTS:
                pass

    def _iter_missing_intermediary_entries(self, dn):
        """Split and yield all intermediary entries for a given DN"""

        dn_list = dn.split(",")
        for i in range(len(dn_list)):
            intermediary_dn = ",".join(dn_list[-1 - i :]).strip()
            if intermediary_dn in (
                dn,  # don't consider the dn we're are splitting as intermediary
                # the following are bootstrapped and should be ignored
                "ou=gouv" + self.base,
                self.base,
                "dc=fr",
            ):
                continue
            yield intermediary_dn

    def _iter_ldap_entries(self, base):
        """Perform an LDAP SEARCH on the entire tree and yield the results"""

        if self.dry_run:
            log.debug("-- DRY-RUN -- SEARCH in %s", base)
            return None

        # Check if base exists
        try:
            self.conn.search_s(base=base, scope=ldap.SCOPE_BASE)
        except ldap.NO_SUCH_OBJECT:
            return None

        # Base exists, yield the entries
        for dn, entry in self.conn.search_s(base=base, scope=ldap.SCOPE_SUBTREE):
            entry = {attr.lower(): value for attr, value in entry.items()}
            yield dn, entry

    def find_base_from_dn(self, dn):
        """Use known suffixes to find the corresponding base for a given DN"""

        dn = dn.lower()
        base = self.base  # default if not found

        for suffix in self.handled_suffixes:
            if dn.endswith(suffix.lower()):
                base = suffix
                break

        return base

    def get_entry(self, dn):
        """Get an entry from our cache and initialize cache if empty"""

        dn = dn.lower()
        base = self.find_base_from_dn(dn)

        if base == self.base:
            # We didn't find a handled suffix, search for DN directly
            if dn not in self.cache[base]:
                dn, entry = self.conn.search_s(base=dn, scope=ldap.SCOPE_BASE)[0]
                dn = dn.lower()
                self.update_cache(dn, entry)
        elif not self.cache[base]:
            # We found a base but it hasn't been initialized
            self.init_cache(base)

        return self.cache[base].get(dn)

    def update_cache(self, dn, entry):
        """Invoked each time an entry is added or modified"""

        dn = dn.lower()
        base = self.find_base_from_dn(dn)

        if base != self.base and not self.cache[base]:
            # We found a base but it hasn't been initialized
            self.init_cache(base)

        self.cache[base][dn] = entry
        self.remainder[base].discard(dn)

    def init_cache(self, base):
        """Populate the cache with existing DNs"""

        assert base == base.lower()

        # Prevent accidental double-initialization of cache
        if base in self.cache and self.cache[base] is not None:
            log.warning("Cache already initialized for base '%s'", base)
            return

        _cache = {}
        _remainder = set()
        for dn, entry in self._iter_ldap_entries(base):
            dn = dn.lower()
            _cache[dn] = entry
            _remainder.add(dn)

        self.cache[base] = _cache
        self.remainder[base] = _remainder
        log.info("Loaded %s items in cache for %s", len(self.cache[base]), base)


def entries_are_equal(entry1, entry2, ignore_attr_types=("objectclass",)):
    """Compare two LDAP entries, which are dicts, ignoring <ignore_attr_types>

    Adapted from https://stackoverflow.com/questions/10480806/compare-dictionaries-ignoring-specific-keys#answer-10480904
    """

    if not ignore_attr_types and "objectclass" in entry1:
        ignore_attr_types = get_ignore_attr_types(entry1)
    if "objectclass" not in ignore_attr_types:
        ignore_attr_types.append("objectclass")

    for attr1, val1 in entry1.items():
        if attr1 not in ignore_attr_types and (attr1 not in entry2 or entry2[attr1] != val1):
            return False
    for attr2 in entry2:
        if attr2 not in ignore_attr_types and attr2 not in entry1:
            return False
    return True
