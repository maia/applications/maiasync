"""Custom utils to handle LDIF"""

import logging
import io
import warnings
from ldif import LDIFParser  # type: ignore

import lib.psql as model

log = logging.getLogger()


def iter_ldif_entries(ldif_input):
    """yield (lineno, LDIF entry) tuples

    Instead of parsing entire LDIF files, which would force giving up the entire
    treatment on errors, yield individual LDIF entries, so they can be handled
    separately.
    """

    with open(ldif_input, "r") as ldif_file:
        curr_entry_line = 0
        curr_entry_content = ""
        for i, line in enumerate(ldif_file.readlines()):
            if line.startswith("dn:"):
                if curr_entry_line > 0:
                    yield curr_entry_line, curr_entry_content.strip()
                    curr_entry_content = ""
                curr_entry_line = i + 1
            if len(line.strip()) == 0:
                continue
            curr_entry_content += line
    if curr_entry_content:
        # last entry in file, only if there's something to return
        yield curr_entry_line, curr_entry_content.strip()


# List of LDAP attributes having aliases.
# Here is a way to obtain this list:
#   ldapsearch -LLL -o ldif-wrap=no -Y EXTERNAL -H ldapi:/// -b cn=schema,cn=config | grep '^olcAttributeTypes: .* NAME (.*) ' | grep -o ' NAME .* DESC
map_ldap_aliases = {
    "aliasedEntryName".lower(): "aliasedObjectName",
    "saslAuthzFrom".lower(): "authzFrom",
    "saslAuthzTo".lower(): "authzTo",
    "countryName".lower(): "c",
    "commonName".lower(): "cn",
    "friendlyCountryName".lower(): "co",
    "domainComponent".lower(): "dc",
    "favouriteDrink".lower(): "drink",
    "emailAddress".lower(): "email",
    "pkcs9email".lower(): "email",
    "fax".lower(): "facsimileTelephoneNumber",
    "gn".lower(): "givenName",
    "homeTelephoneNumber".lower(): "homePhone",
    "localityName".lower(): "l",
    "rfc822Mailbox".lower(): "mail",
    "mobileTelephoneNumber".lower(): "mobile",
    "organizationName".lower(): "o",
    "organizationalUnitName".lower(): "ou",
    "pagerTelephoneNumber".lower(): "pager",
    "surname".lower(): "sn",
    "streetAddress".lower(): "street",
    "stateOrProvinceName".lower(): "st",
    "userid".lower(): "uid",
}

# List of LDAP attributes that are DNs.
# Here is a way to obtain this list:
#   ldapsearch -LLL -o ldif-wrap=no -Y EXTERNAL -H ldapi:/// -b cn=schema,cn=config | grep '^olcAttribute.*EQUALITY.*distinguishedNameMatch'
ldap_dn_equality_attrs = {
    "manager".lower(),
    "documentAuthor".lower(),
    "secretary".lower(),
    "associatedName".lower(),
    "dITRedirect".lower(),
}


def canonicalize_entry(dn, entry):
    """
    - lowercase DN
    - remove extra spaces surrounding DN's components
    - rename LDAP attributes from their respective aliases (i.e.: countryname -> c)
    """
    # correct leading/trailing whitespace around dn parts
    clean_dn = dn.lower()
    clean_dn = "=".join(item.strip() for item in clean_dn.split("="))
    clean_dn = ",".join(item.strip() for item in clean_dn.split(","))

    clean_entry = {}
    for k, v in entry.items():
        # We want case insensitive keys, so we force them to lowercase
        k = k.strip().lower()

        # Convert alias attribute name to standard attribute, if any
        k = map_ldap_aliases.get(k, k)

        # Sometimes, LDIF files contain spurious spaces, let's trim them!
        if isinstance(v, (list, tuple)):
            tmp = []
            for _val in v:
                if not _val.strip():
                    continue
                tmp.append(_val.strip())
            v = list(tmp)
        elif isinstance(v, (str, bytes)):
            v = v.strip()
        else:
            log.error("Cannot clean attr %s. Unsupported data type %s.", k, type(v))

        # Clean whitespace for DNs in some attributes, like 'manager'
        if k in ldap_dn_equality_attrs:
            if isinstance(v, str):
                v = "=".join(item.strip() for item in v.split("="))
                v = ",".join(item.strip() for item in v.split(","))
            elif isinstance(v, bytes):
                v = b"=".join(item.strip() for item in v.split(b"="))
                v = b",".join(item.strip() for item in v.split(b","))
            if isinstance(v, (list, tuple)):
                tmp = []
                for _val in v:
                    if isinstance(_val, str):
                        _val = "=".join(item.strip() for item in _val.split("="))
                        _val = ",".join(item.strip() for item in _val.split(","))
                    elif isinstance(_val, bytes):
                        _val = b"=".join(item.strip() for item in _val.split(b"="))
                        _val = b",".join(item.strip() for item in _val.split(b","))
                    else:
                        log.error("Cannot clean attr %s. Unsupported data type %s.", k, type(_val))
                    tmp.append(_val)
                v = list(tmp)
            else:
                log.error("Cannot clean attr %s. Unsupported data type %s.", k, type(v))

        # Remove attributes with empty value
        if not v:
            continue
        clean_entry[k] = v

    return clean_dn, clean_entry


def parse_ldif(filename, batch_size=None, exc_handler=None):
    """Parse LDIF entries and yield parsed dicts"""

    entry_readcount = 0
    entry_failcount = 0

    for lineno, raw_entry in iter_ldif_entries(filename):
        fake_ldif_file = io.StringIO(raw_entry)
        try:
            entry_readcount += 1
            yielding_parser = YieldingLdifParser(fake_ldif_file)
            yielding_parser.parse()
            for dn, entry in yielding_parser.entries:
                yield canonicalize_entry(dn, entry)
            if batch_size and entry_readcount % batch_size == 0:
                log.info("%s entries read so far...", entry_readcount)
        except ValueError as e:
            fixed_raw_entry = exc_handler(e, raw_entry)
            if exc_handler and fixed_raw_entry:
                fake_ldif_file = io.StringIO(fixed_raw_entry)
                yielding_parser = YieldingLdifParser(fake_ldif_file)
                try:
                    yielding_parser.parse()
                    for dn, entry in yielding_parser.entries:
                        yield canonicalize_entry(dn, entry)
                except ValueError as e_bis:
                    entry_failcount += 1
                    error_msg = f"Failed to parse ldif entry at line {lineno}, Reason: {e}"
                    dn_for_error_msg = raw_entry.split("\n")[0]
                    error_msg += f"\n  {dn_for_error_msg}"
                    warnings.warn(error_msg, LDIFNeedsCorrection)
            else:
                entry_failcount += 1
                error_msg = f"Failed to parse ldif entry at line {lineno}, Reason: {e}"
                dn_for_error_msg = raw_entry.split("\n")[0]
                error_msg += f"\n  {dn_for_error_msg}"
                warnings.warn(error_msg, LDIFNeedsCorrection)
        except Exception:
            log.critical("Unexpected error.", exc_info=True)
            break


class YieldingLdifParser(LDIFParser):
    """
    Our own basic LDIF parser.

    This class is a hack to work around two limitations of python-ldap's LDIF parser:
    1. parsing LDIF breaks the whole parsing whenever a malformed entry is encountered,
       but we want to be able to decide by ourself if such an error is acceptable or not
    2. python-ldap does not provide a pythonic way for iterating over LDIF entries

    So, this class does the parsing, and for each entry (call of self.handle), stores it
    into self.entries.
    """

    def __init__(self, input_ldif):
        self.entries = []
        LDIFParser.__init__(
            self,
            input_ldif,
            ignored_attr_types=[
                "teletexTerminalIdentifier",
                "aci",
                "roomNumber",
                "gender",
                "jpegPhoto",
                "freeFormName",
            ],
        )

    def handle(self, dn, entry):
        self.entries.append((dn, entry))


class LdifToPsqlTableConverter:
    """Convert LDIF data into PSQLTable items"""

    def __init__(self, criteria):
        self.criteria = criteria

    def iter_items(self, dn, entry):
        """Yield converted PSQLTable objects.

        A given LDIF entry can contain multiple PSQLTable objects.
        """

        for handle in self.criteria["is_person"]:
            if handle in entry["objectclass"]:
                dbitem_person = model.Person(dn, data_supply_id=self.criteria["data_supply"])
                for dbcol, ldap_attr in self.criteria["person"].items():
                    if ldap_attr not in entry:
                        continue
                    dbitem_person.data[dbcol] = entry[ldap_attr][0].decode()
                    # manager attribute is a full DN, we need to extract only the email address
                    if dbcol == "manager":
                        tmp = dbitem_person.data[dbcol]
                        tmp = tmp.split("=")[1]
                        tmp = tmp.split(",")[0]
                        dbitem_person.data[dbcol] = tmp
                # Let's keep a local copy of dbitem's data before yield, because
                # it's content may be modified outside.
                dbitem_person_data = dbitem_person.data.copy()
                yield dbitem_person

                # Create a NEO_USER per PERSON
                dbitem_neouser = model.NeoUser(dn, data_supply_id=self.criteria["data_supply"])
                dbitem_neouser.data["USERNAME"] = dbitem_person_data["IDENTITY_NUMBER"]
                dbitem_neouser.data["person"] = dbitem_person_data["IDENTITY_NUMBER"]
                yield dbitem_neouser

                # Data found for table PHONE for a PERSON
                # SELECT "ID", "LABEL" FROM "PHONE_CATEGORY" -- 1=Fixe, 2=Télécopie, 3=Mobile
                for i, phone_type in enumerate(
                    ("telephonenumber", "facsimiletelephonenumber", "mobile")
                ):
                    if phone_type in entry:
                        for attr_val in entry[phone_type]:
                            dbitem_phone = model.Phone(dn, data_supply_id=self.criteria["data_supply"])
                            dbitem_phone.data["NUMBER"] = attr_val.decode()
                            dbitem_phone.data["PHONE_CATEGORY_ID"] = i + 1
                            dbitem_phone.data["person"] = dbitem_person_data["IDENTITY_NUMBER"]
                            yield dbitem_phone

                # Data found for table POSTAL_ADDRESS for a PERSON
                if (
                    "street" in entry
                    or "l" in entry
                    or "c" in entry
                    or "postalcode" in entry
                    or "localityname" in entry
                    or "streetaddress" in entry
                ):
                    dbitem_addr = model.PostalAddress(dn, data_supply_id=self.criteria["data_supply"])

                    # STREET
                    street = ""
                    if "street" in entry:
                        street = entry["street"][0].decode()
                    elif "streetaddress" in entry:
                        street = entry["streetaddress"][0].decode()
                    else:
                        pass
                    street = street.replace("$", " ")
                    street = street.strip()
                    if street:
                        dbitem_addr.data["STREET"] = street

                    # ZIP_CODE
                    zip_code = ""
                    if "postalcode" in entry:
                        zip_code = entry["postalcode"][0].decode()
                    zip_code = zip_code.strip()
                    if zip_code:
                        dbitem_addr.data["ZIP_CODE"] = zip_code

                    # TOWN
                    town = ""
                    if "l" in entry:
                        town = entry["l"][0].decode()
                    elif "localityname" in entry:
                        town = entry["localityname"][0].decode()
                    town = town.strip()
                    if town:
                        dbitem_addr.data["TOWN"] = town

                    # COUNTRY
                    country = ""
                    if "c" in entry:
                        country = entry["c"][0].decode()
                    country = country.strip()
                    if country:
                        dbitem_addr.data["COUNTRY"] = country

                    dbitem_addr.data["person"] = dbitem_person_data["IDENTITY_NUMBER"]
                    yield dbitem_addr
                break  # only yield once per criteria

        for handle in self.criteria["is_structure"]:
            if handle in entry["objectclass"]:
                dbitem_structure = model.Structure(
                    dn, data_supply_id=self.criteria["data_supply"]
                )

                # Special case for top-level STRUCTUREs
                if "departmentnumber" not in entry and "ou" in entry:
                    log.info(
                        "Auto-created missing attribute departmentNumber: %s",
                        entry["ou"],
                    )
                    entry["departmentnumber"] = entry["ou"]

                for dbcol, ldap_attr in self.criteria["structure"].items():
                    if ldap_attr not in entry:
                        continue
                    dbitem_structure.data[dbcol] = entry[ldap_attr][0].decode()
                # let's keep a copy of dbitem's data before yield, because it
                # may be altered in calling code
                dbitem_structure_data = dbitem_structure.data.copy()
                yield dbitem_structure

                # Data found for table PHONE for a STRUCTURE
                for i, phone_type in enumerate(
                    ("telephonenumber", "facsimiletelephonenumber", "mobile")
                ):
                    if phone_type in entry:
                        for attr_val in entry[phone_type]:
                            dbitem_phone = model.Phone(dn, data_supply_id=self.criteria["data_supply"])
                            dbitem_phone.data["NUMBER"] = attr_val.decode()
                            dbitem_phone.data["PHONE_CATEGORY_ID"] = i + 1
                            dbitem_phone.data["structure"] = dbitem_structure_data["FULL_CODE"]
                            yield dbitem_phone

                # Data found for table POSTAL_ADDRESS for a STRUCTURE
                if (
                    "street" in entry
                    or "l" in entry
                    or "c" in entry
                    or "postalcode" in entry
                    or "localityname" in entry
                ):
                    dbitem_addr = model.PostalAddress(dn, data_supply_id=self.criteria["data_supply"])

                    # STREET
                    street = ""
                    if "street" in entry:
                        street = entry["street"][0].decode()
                    elif "streetaddress" in entry:
                        street = entry["streetaddress"][0].decode()
                    else:
                        pass
                    street = street.replace("$", " ")
                    street = street.strip()
                    if street:
                        dbitem_addr.data["STREET"] = street

                    # ZIP_CODE
                    zip_code = ""
                    if "postalcode" in entry:
                        zip_code = entry["postalcode"][0].decode()
                    zip_code = zip_code.strip()
                    if zip_code:
                        dbitem_addr.data["ZIP_CODE"] = zip_code

                    # TOWN
                    town = ""
                    if "l" in entry:
                        town = entry["l"][0].decode()
                    elif "localityname" in entry:
                        town = entry["localityname"][0].decode()
                    town = town.strip()
                    if town:
                        dbitem_addr.data["TOWN"] = town

                    # COUNTRY
                    country = ""
                    if "c" in entry:
                        country = entry["c"][0].decode()
                    country = country.strip()
                    if country:
                        dbitem_addr.data["COUNTRY"] = country

                    dbitem_addr.data["structure"] = dbitem_structure_data["FULL_CODE"]
                    yield dbitem_addr

                break  # only yield once per criteria

        for handle in self.criteria["is_shared_mailbox"]:
            if handle in entry["objectclass"]:
                dbitem = model.SharedMailbox(
                    dn, data_supply_id=self.criteria["data_supply"]
                )
                for dbcol, ldap_attr in self.criteria["shared_mailbox"].items():
                    if ldap_attr not in entry:
                        continue
                    dbitem.data[dbcol] = entry[ldap_attr][0].decode()
                yield dbitem
                break  # only yield once per criteria


class MaiaLDIFCleaner:
    """Cleanup LDIF data to be ready for sending to LDAP"""

    def __init__(self, criteria):
        self.criteria = criteria

    def iter_items(self, dn, entry):
        """Yield corrected LDIF entries with the attr_type to ignore"""

        # Correct missing RDNs
        rdn = dn.split(",")[0]
        rdn_attr, rdn_val = rdn.split("=")
        rdn_attr = rdn_attr.lower()
        rdn = {rdn_attr: [rdn_val.encode()]}
        if rdn_attr not in entry:
            entry.update(rdn)

        # Correct bad suffixes, that are found in some LDIF files,
        # and need to be replaced by our suffix, with the provider as data_supply
        for bad_suffix in self.criteria["bad_suffixes"]:
            if dn.lower().endswith(bad_suffix.lower()):
                dn = dn[: -len(bad_suffix)]
                dn += f"ou={self.criteria['data_supply']},ou=gouv,dc=service-public,dc=fr"
                if "ou" not in entry:
                    entry["ou"] = []
                insert_ou = True
                for ou in entry["ou"]:
                    if ou.lower() == self.criteria["data_supply"].encode().lower():
                        insert_ou = False
                        break
                if insert_ou:
                    entry["ou"].append(self.criteria["data_supply"].encode())

        # Correct some objectclasses that would be redundent to add to the schema
        objectclass_corrections = {
            "obmUser": "frGovPerson",
            "perditionPopmap": "frGovPerson",
            "fimadFrGovPerson": "frGovPerson",
            "miFrGovPerson": "frGovPerson",
            "fimadFrGovOrganizationalRole": "frGovOrganizationalRole",
        }
        for objectclass in list(entry["objectclass"]):
            if objectclass.decode() in objectclass_corrections:
                entry["objectclass"].remove(objectclass)
                if (
                    objectclass_corrections[objectclass.decode()].encode()
                    not in entry["objectclass"]
                ):
                    entry["objectclass"].append(
                        objectclass_corrections[objectclass.decode()].encode()
                    )

        # Some entries have only core objectclasses (like organizationalUnit)
        # so they can't respect the schema
        if (
            b"organizationalUnit" in entry["objectclass"]
            and b"frGovOrganizationalUnit" not in entry["objectclass"]
        ):
            entry["objectclass"].append(b"frGovOrganizationalUnit")
        elif (
            b"organizationalPerson" in entry["objectclass"]
            and b"frGovPerson" not in entry["objectclass"]
        ):
            entry["objectclass"].append(b"frGovPerson")
        elif (
            b"organizationalRole" in entry["objectclass"]
            and b"frGovOrganizationalRole" not in entry["objectclass"]
        ):
            entry["objectclass"].append(b"frGovOrganizationalRole")

        # Add the provider's MaiaDataSupply attribute
        if self.criteria["data_supply"]:
            entry["maiadatasupply"] = [self.criteria["data_supply"].encode()]

        # Correct attributes that create Object class violations
        # PERSON
        for objectclass in (b"frGovPerson", b"organizationalPerson"):
            if objectclass in entry["objectclass"] and "cn" not in entry:
                entry["cn"] = [
                    f"{entry['sn'][0].decode()} {entry['givenname'][0].decode()}".encode()
                ]
        # STRUCTURE
        for objectclass in (b"frGovOrganizationalUnit", b"organizationalUnit"):
            if objectclass in entry["objectclass"] and "cn" in entry:
                entry.pop("cn")

        log.debug(f"Handling {dn}\n    " +
            "\n    ".join(
                f"{attr}: {val.decode()}"
                for attr, value in entry.items()
                for val in value
            )
        )

        ignore_attr_types = get_ignore_attr_types(entry)

        yield dn, entry, ignore_attr_types


def get_ignore_attr_types(entry):
    """Build a list of ignore_attr_types"""

    ignore_attr_types = []
    # PERSON
    if b"organizationalPerson" in entry["objectclass"]:
        ignore_attr_types += [
            "c",
        ]
    if b"frGovPerson" in entry["objectclass"]:
        ignore_attr_types += [
            "parentou",
            "mailboxtype",
            "publication",
        ]
    # SHARED_MAILBOX
    if b"frGovOrganizationalRole" in entry["objectclass"]:
        ignore_attr_types += [
            "givenname",
            "mobile",
            "title",
            "parentou",
            "mailboxtype",
        ]
    # STRUCTURE
    if b"frGovOrganizationalUnit" in entry["objectclass"]:
        ignore_attr_types += [
            "givenname",
            "manager",
            "mobile",
            "title",
            "c",
            "parent_ou",
        ]

    return ignore_attr_types


class InputFileNeedsCorrection(Warning):
    """Warning for when a given input file needs correcting.

    We can log this with needed information to send to the provider for correction.
    This class can be subclassed by LDIFNeedsCorrection or CSVNeedsCorrection
    """

    def __init__(self, message):
        self.message = message
        super().__init__(message)


class LDIFNeedsCorrection(InputFileNeedsCorrection):
    """Warning for when a given LDIF file needs correcting."""

    def __init__(self, message):
        self.message = message
        super().__init__(message)
