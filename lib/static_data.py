"""All the static data, which is common to all data inputs"""

from lib import psql


def prepopulate_database(pg_connector):
    """These seem to be manual 'once and for all' inserts"""

    data_supply = (
        ("ASP", "Annuaire des services publics"),
        ("PM", "Premier ministre"),
        ("MASA", "Ministère de l'Agriculture et de la Souveraineté alimentaire"),
        ("MA", "Ministère des Armées"),
        ("MC", "Ministère de la Culture"),
        ("MEFSIN", "Ministère de l'Économie, des Finances et de la Souveraineté industrielle et numérique"),
        ("MENJ", "Ministère de l'Éducation nationale et de la Jeunesse"),
        ("MESR", "Ministère de l'Enseignement supérieur et de la Recherche"),
        ("MEAE", "Ministère de l'Europe et des Affaires étrangères"),
        ("MIOM", "Ministère de l'Intérieur et des Outre-mer"),
        ("MJ", "Ministère de la Justice"),
        ("MSP", "Ministère de la Santé et de la Prévention"),
        ("MSAPH", "Ministère des Solidarités, de l'Autonomie et des Personnes handicapées"),
        ("MSJOP", "Ministère des Sports et des Jeux Olympiques et Paralympiques"),
        ("MTFP", "Ministère de la Transformation et de la Fonction publiques"),
        ("MTECT", "Ministère de la Transition écologique et de la Cohésion des territoires"),
        ("MTE", "Ministère de la Transition énergétique"),
        ("MTPEI", "Ministère du Travail, du Plein emploi et de l'Insertion"),
        ("MAIA", "MAIA"),
        ("ALCD", "Administration locale conseil départemental"),
        ("ALCR", "Administration locale conseil régional"),
    )
    for code, label in data_supply:
        dbitem = psql.DataSupply(code=code, label=label, active=1)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    demand_statut = (
        ("OUV", "Ouvert"),
        ("ENC", "En cours de traitement"),
        ("REA", "Réalisé"),
        ("REF", "Refusé"),
    )
    for code, label in demand_statut:
        dbitem = psql.DemandStatut(code=code, label=label)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    person_fields = (
        ("Nom", "NOM", "lastName"),
        ("Prénom", "PRE", "firstName"),
        ("Courriel", "COU", "mail"),
        ("Téléphones", "TEL", "phoneNumber.number"),
        ("Fonction", "FONC", "function"),
        ("Responsable", "RESP", "manager.identityNumber"),
        ("Service", "SER", "structure.fullCode"),
        ("Adresse postale" ,"ADR", "postalAddress"),
    )
    for label, code, field in person_fields:
        dbitem = psql.PersonFields(label=label, code=code, field=field)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    phone_category = (
        ("FIXE", "Fixe"),
        ("FAX", "Télécopie"),
        ("MOBILE", "Mobile"),
    )
    for code, label in phone_category:
        dbitem = psql.PhoneCategory(code=code, label=label, active=1)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    population = (
        ("AUTHENTICATED", "authenticated"),
        ("ANONYMOUS", "anonymous"),
    )
    for code, label in population:
        dbitem = psql.Population(code=code, label=label, active=1)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    profile = (
        ("ADMIN", "Administrateur général"),
        ("AGENT", "Agent"),
        ("GESTIONNAIRE", "gestionnaire"),
        ("NEOSYNC", "Admin NeoSync"),
        ("AGENT-A", "Agent audité"),
        ("MINISTRY-ADMIN", "Administrateur point d'alimentation"),
        ("SUPER-ADMIN", "Super Administrateur"),
        ("DATASUPPLY-ADMIN", "data supply Administrateur"),
    )
    for code, label in profile:
        dbitem = psql.Profile(code=code, label=label, active=1)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    structure = (
        ("agriculture", "Agriculture", None, "http://www.agriculture.gouv.fr"),
        ("culture", "Culture", None, "http://www.culturecommunication.gouv.fr"),
        ("DEFENSE", "Défense", None, "http://www.defense.gouv.fr"),
        ("departements", "Départements", None, None),
        ("diplomatie", "Europe et Affaires étrangères", None, "http://www.diplomatie.gouv.fr"),
        ("ecologie", "Transition écologique et cohésion des territoires", None, "http://www.developpement-durable.gouv.fr"),
        ("education", "Éducation nationale (admin. centrale)", None, "http://www.education.gouv.fr"),
        ("JUSTICE", "Justice", None, "http://www.justice.gouv.fr"),
        ("mes-co", "SG Social", None, None),
        ("mes-sa", "Santé", None, "http://www.sante-jeunesse-sports.gouv.fr"),
        ("mes-so", "Solidarités", None, None),
        ("mes-tr", "Travail", None, "http://www.travail-solidarite.gouv.fr/"),
        ("MINEFI", "Économie, finances et souveraineté industrielle et numérique", None, "http://www.minefe.gouv.fr/"),
        ("MININT", "Intérieur et Outre-Mer", None, None),
        ("PM", "Services PM", "Service du Premier ministre", "http://www.premier-ministre.gouv.fr"),
        ("regions", "Régions (admin. territoriale)", "Régions", None),
    )
    maia_data_supply_id = pg_connector.get_data_supply_id("MAIA")
    for full_code, code, label, web_portal in structure:
        dbitem = psql.Structure(
            dn=None,
            full_code=full_code,
            code=code,
            label=label,
            web_portal=web_portal,
            data_supply_id=maia_data_supply_id,
        )
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    structure_fields = (
        ("Identifiant", "ID", "fullCode"),
        ("Nom", "NOM", "code"),
        ("Description", "DESC", "label"),
        ("Courriel", "MAIL", "mail"),
        ("Téléphone", "TEL", "phoneNumber.number"),
        ("Service de rattachement", "SDR", "parent.code"),
        ("Adresse postale", "AP", "postalAddress"),
        ("Site portail", "SP", "webPortal"),
    )
    for label, code, field in structure_fields:
        dbitem = psql.StructureFields(label=label, code=code, field=field)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)

    survey_question = {
        "Q1": "Avez-vous des suggestions d'amélioration ?",
    }
    for code, question in survey_question.items():
        dbitem = psql.SurveyQuestion(code=code, question=question, active=1)
        if dbitem.tablename not in pg_connector.stats:
            pg_connector.stats[dbitem.tablename] = {
                "read": 0, "insert": 0, "update": 0, "delete": 0,
            }
        pg_connector.stats[dbitem.tablename]["read"] += 1
        pg_connector.upsert(dbitem)
