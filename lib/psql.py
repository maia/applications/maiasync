"""All the classes representing the SQL connector and SQL tables"""

from collections import namedtuple
import logging
from datetime import datetime
import psycopg2  # type: ignore

log = logging.getLogger()


def _my_hash(items):
    if items is None:
        items = {"": ""}
    assert isinstance(items, dict)
    cache_key = "#"
    for k in sorted(items):
        v = items.get(k) or "-"
        cache_key += k + "#" + str(v) + " "
    return cache_key.strip()


class PSQLConnector:
    def __init__(self, host, port, user, password, database, dry_run=False):
        self.dry_run = dry_run
        if not dry_run:
            self.conn = psycopg2.connect(
                host=host, port=port, user=user, database=database, password=password
            )
        self.stats = {}
        self.cache = {
            "special_for_person": {
                "PHONE": {},
                "POSTAL_ADDRESS": {},
            },
            "special_for_structure": {
                "PHONE": {},
                "POSTAL_ADDRESS": {},
            },
        }
        self.widecache = {}
        self.remainder = {}

    def get_schema_version(self):
        query = "SELECT MAX(version::int) FROM maia_db_version"
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(query)
                return cur.fetchone()[0]
        return None

    def upsert(self, item):
        """
        :param PSQLTable item: an object holding data to insert or update.
        """

        item.make_truncations()
        query = item.compute_upsert_query()

        if self.dry_run:
            log.debug("-- DRY-RUN -- %s", query)
            return None
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(f'SELECT last_value from "SEQ_{item.tablename}";')
                last_value_id = cur.fetchone()[0]
                try:
                    cur.execute(query, item.data)
                except Exception as e:
                    log.debug(
                        "Failed to execute query:\n  %s\n  with params: %s",
                        query,
                        item.data,
                        exc_info=True,
                    )
                    raise e
                returned_from_query = cur.fetchone()
                if item.tablename in ("PERSON", "SHARED_MAILBOX", "STRUCTURE", "PHONE", "POSTAL_ADDRESS", "NEO_USER"):
                    log.debug("Run query\n " + cur.query.decode())

        if returned_from_query is None:
            self.update_cache(item, row_id=None)
            return None  # DB row already exists and was unchanged

        # Update was made, figure out if it was an insert or update
        row_id = returned_from_query[0]
        if row_id == last_value_id + 1:
            self.stats[item.tablename]["insert"] += 1
            log.debug('Inserted into %s, "ID" = %s', item.tablename, row_id)
        else:
            self.stats[item.tablename]["update"] += 1
            log.debug('Updated %s with "ID" = %s', item.tablename, row_id)

        self.update_cache(item, row_id)

        return row_id

    def delete(self, table, rowid):
        query = f"""DELETE FROM "{table}" WHERE "ID" = {rowid};"""
        if self.dry_run:
            log.debug("-- DRY-RUN -- %s", query)
            return
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(query)

    def delete_where(self, table, filters):
        if not filters:
            raise Exception(
                "Missing argument 'filters'. You probably don't really want to delete all table {table}."
            )
        where_clause = " AND ".join(f'"{k}" = {v}' for k, v in filters.items())
        query = f"""DELETE FROM "{table}" WHERE {where_clause};"""

        if self.dry_run:
            log.debug("-- DRY-RUN -- %s", query)
            return
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(query)

    def delete_where_in(self, table, column, ids=()):
        query = f"""DELETE FROM "{table}" WHERE "{column}" IN ({', '.join(ids)});"""

        if self.dry_run:
            log.debug("-- DRY-RUN -- %s", query)
            return
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(query)

    def update_where(self, table, updates, filters):
        if not filters:
            raise Exception("Missing argument 'filters'. You probably don't really want to update all rows in table {table}.")
        update_clause = ", ".join(f'"{k}" = {v}' for k, v in updates.items())
        where_clause = " AND ".join(f'"{k}" = {v}' for k, v in filters.items())
        query = f"""UPDATE "{table}" SET {update_clause} WHERE {where_clause};"""

        if self.dry_run:
            log.debug("-- DRY-RUN -- %s", query)
            return
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(query)

    def update_where_in(self, table, column, ids=(), updates={}):
        update_clause = ", ".join(f'"{k}" = {v}' for k, v in updates.items())
        query = f"""UPDATE "{table}" SET {update_clause} WHERE "{column}" IN ({', '.join(ids)});"""

        if self.dry_run:
            log.debug("-- DRY-RUN -- %s", query)
            return
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(query)

    def _iter_ids_from(self, table, data_supply_id, columns):
        if isinstance(columns, str):
            columns = (columns,)

        select_clause = ", ".join(f't."{col}"' for col in columns)
        query = f'SELECT DISTINCT t."ID", {select_clause} \nFROM "{table}" t '
        if data_supply_id and table in ("PERSON", "SHARED_MAILBOX", "STRUCTURE"):
            query += f" WHERE t.\"DATA_SUPPLY_ID\" = '{data_supply_id}'"
        elif data_supply_id and table == "PHONE":
            query += f'\nLEFT JOIN "PERSON"    p ON t."PERSON_ID" = p."ID"    AND p."DATA_SUPPLY_ID" = {data_supply_id} AND p."ID" IS NOT NULL '
            query += f'\nLEFT JOIN "STRUCTURE" s ON t."STRUCTURE_ID" = s."ID" AND s."DATA_SUPPLY_ID" = {data_supply_id} AND s."ID" IS NOT NULL '
            query += '\nWHERE p."ID" IS NOT NULL OR s."ID" IS NOT NULL'
        elif data_supply_id and table == "POSTAL_ADDRESS":
            query += f'\nLEFT JOIN "PERSON"    p ON t."PERSON_POSTAL_ADDRESS_ID" = p."ID"    AND p."DATA_SUPPLY_ID" = {data_supply_id} AND p."ID" IS NOT NULL '
            query += f'\nLEFT JOIN "STRUCTURE" s ON t."STRUCTURE_POSTAL_ADDRESS_ID" = s."ID" AND s."DATA_SUPPLY_ID" = {data_supply_id} AND s."ID" IS NOT NULL '
            query += '\nWHERE p."ID" IS NOT NULL OR s."ID" IS NOT NULL'
        query += ";"
        log.debug(query)
        with self.conn:
            with self.conn.cursor() as cur:
                cur.execute(query)
                for row in cur:
                    yield row

    def get_id_from(self, table, data_supply_id, keyval):
        """Get a row_id from our cache and initialize cache if empty"""

        if table not in self.cache:
            self.init_cache(table, data_supply_id, keyval.keys())
        _cache_key = _my_hash(keyval)
        res = self.cache[table].get(_cache_key)
        if not res:
            if table not in self.widecache:
                self.init_widecache(table, keyval.keys())
            res = self.widecache[table].get(_cache_key)
            if res:
                log.debug(
                    "Needed to search accross the whole table %s to find %s => %s",
                    table,
                    keyval,
                    res,
                )
        return res

    def update_cache(self, item, row_id):
        """Invoked each time a row is inserted or updated into DB"""
        keyval = {}
        for column in item.conflict_columns:
            keyval[column.name] = item.data.get(column.name)

        if item.tablename not in self.cache:
            self.init_cache(
                item.tablename, item.data.get("DATA_SUPPLY_ID"), keyval.keys()
            )
        _cache_key = _my_hash(keyval)
        if row_id is not None:
            self.cache[item.tablename][_cache_key] = row_id

        if item.tablename in ["PERSON", "SHARED_MAILBOX", "STRUCTURE", "PHONE", "POSTAL_ADDRESS"]:
            self.remainder[item.tablename].discard(_cache_key)

    def init_cache(self, table, data_supply_id, columns):
        """Create a new cache for a given table, with the existing database values"""
        columns = sorted(columns)
        self.cache[table] = {}
        self.remainder[table] = set()
        for row in self._iter_ids_from(table, data_supply_id, columns):
            row_id = row[0]
            keyval = {}
            for i, col in enumerate(columns):
                keyval[col] = row[i + 1]
            _cache_key = _my_hash(keyval)
            self.cache[table][_cache_key] = row_id
            if table in ["PERSON", "SHARED_MAILBOX", "STRUCTURE", "PHONE", "POSTAL_ADDRESS"]:
                self.remainder[table].add(_cache_key)

            # also update special caches (cascading caches) for phones and postal addresses
            if table == "PHONE":
                phone_category_id = keyval["PHONE_CATEGORY_ID"]
                phone_number = keyval["NUMBER"]
                if keyval["PERSON_ID"] is not None:
                    person_id = int(keyval["PERSON_ID"])
                    if person_id not in self.cache["special_for_person"][table]:
                        self.cache["special_for_person"][table][person_id] = {}
                    if phone_category_id not in self.cache["special_for_person"][table][person_id]:
                        self.cache["special_for_person"][table][person_id][phone_category_id] = set()
                    self.cache["special_for_person"][table][person_id][phone_category_id].add(phone_number)
                elif keyval["STRUCTURE_ID"] is not None:
                    structure_id = int(keyval["STRUCTURE_ID"])
                    if structure_id not in self.cache["special_for_structure"][table]:
                        self.cache["special_for_structure"][table][structure_id] = {}
                    if phone_category_id not in self.cache["special_for_structure"][table][structure_id]:
                        self.cache["special_for_structure"][table][structure_id][phone_category_id] = set()
                    self.cache["special_for_structure"][table][structure_id][phone_category_id].add(phone_number)

            elif table == "POSTAL_ADDRESS":
                canonical_address = "{}\n{}\n{}\n{}".format(
                    keyval.get("STREET") or "",
                    keyval.get("ZIP_CODE") or "",
                    keyval.get("TOWN") or "",
                    keyval.get("COUNTRY") or "",
                )
                keyval["canonical_address"] = canonical_address
                if keyval["PERSON_POSTAL_ADDRESS_ID"] is not None:
                    person_id = int(keyval["PERSON_POSTAL_ADDRESS_ID"])
                    if person_id not in self.cache["special_for_person"][table]:
                        self.cache["special_for_person"][table][person_id] = keyval
                elif keyval["STRUCTURE_POSTAL_ADDRESS_ID"] is not None:
                    structure_id = int(keyval["STRUCTURE_POSTAL_ADDRESS_ID"])
                    if structure_id not in self.cache["special_for_structure"][table]:
                        self.cache["special_for_structure"][table][structure_id] = keyval

        log.info(
            "Loaded %s items in cache for table %s", len(self.cache[table]), table
        )

    def init_widecache(self, table, columns):
        """
        Create a new cache for a given table, with all existing database values
        (not restricted to a single DATA_SUPPLY_ID).
        This is needed because some structures or persons may have parent_ids
        exisisting in a different DATA_SUPPLY_ID.
        """
        columns = sorted(columns)
        self.widecache[table] = {}
        for row in self._iter_ids_from(table, None, columns):
            row_id = row[0]
            keyval = {}
            for i, col in enumerate(columns):
                keyval[col] = row[i + 1]
            _cache_key = _my_hash(keyval)
            self.widecache[table][_cache_key] = row_id
        log.debug(
            "Loaded %s items in widecache for table %s", len(self.widecache[table]), table
        )

    def get_data_supply_id(self, data_supply_code):
        if "data_supply" not in self.cache:
            tmp = {}
            query = 'SELECT "ID", "CODE" FROM "DATA_SUPPLY"'
            with self.conn:
                with self.conn.cursor() as cur:
                    cur.execute(query)
                    for row in cur:
                        tmp[row[1]] = row[0]
            self.cache["DATA_SUPPLY"] = tmp
        return self.cache["DATA_SUPPLY"].get(data_supply_code)

    def run_sql(self, queries):
        """Run arbitrary SQL statement(s)"""

        with self.conn:
            with self.conn.cursor() as cur:
                cur.executemany(queries, vars_list=(None,))
                log.debug(cur.query)


CoalesceableColumn = namedtuple("CoalesceableColumn", ["name", "coalesce"])


class PSQLTable:
    """This class should be inherited by classes to represent SQL Tables"""

    def __init__(self):
        self.tablename = None
        self.data = {}
        self.max_value_lengths = {}
        self.conflict_columns = ()
        self.updatable_columns = ()

    def __repr__(self):
        return (
            self.tablename
            + "\n  "
            + "\n  ".join(f"{attr}: {value}" for attr, value in self.data.items())
        )

    def _compute_identifier(self):
        """For debugging purposes only."""
        res = ""
        for col in self.conflict_columns:
            if not isinstance(col, str):
                col = col.name
            val = self.data.get(col, "")
            if val:
                res += f" {col}={val}"
        return res.strip()

    def _compute_cache_keyval(self):
        return {col.name: self.data.get(col.name) for col in self.conflict_columns}

    def _compute_cache_key(self):
        keyval = self._compute_cache_keyval()
        return _my_hash(keyval)

    def make_truncations(self):
        """Make sure the values in <self.data> are not longer than what the schema allows"""

        for column, value in self.data.items():
            if value and column in self.max_value_lengths:
                self.data[column] = value[:self.max_value_lengths[column]]

    def compute_upsert_query(self):
        columns = self.data.keys()
        query_columns = ",".join(f'"{column}"' for column in columns)
        query_values = ",".join(f"%({column})s" for column in columns)
        # Columns used to detect a conflict (must have matching constraints in schema)
        query_conflicts = ",\n   ".join(
            f'"{column.name}"'
            if column.coalesce is None  # not nullable
            else f'COALESCE("{column.name}", {column.coalesce})'
            for column in self.conflict_columns
        )
        # Columns to update (all)
        query_updates = ",\n   ".join(
            f'"{column}" = EXCLUDED."{column}"' for column in columns
        )
        query = (
            f'INSERT INTO "{self.tablename}" ({query_columns})\n'
            f" VALUES ({query_values})\n"
            f" ON CONFLICT (\n   {query_conflicts}\n )\n"
            f" DO UPDATE SET \n   {query_updates} "
        )
        if self.updatable_columns:
            # Columns that are allowed to be updated
            query_updatable = " OR ".join(
                f'"{self.tablename}"."{column.name}" <> EXCLUDED."{column.name}"'
                if column.coalesce is None  # not nullable
                else f'COALESCE("{self.tablename}"."{column.name}", {column.coalesce}) <> EXCLUDED."{column.name}"'
                for column in self.updatable_columns
            )
            query += f"\n WHERE {query_updatable} "
        if "UPDATE_DATE" in self.data:
            query += '\nRETURNING "ID", "UPDATE_DATE";'
        else:
            query += '\nRETURNING "ID";'
        return query


class DataSupply(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "DATA_SUPPLY"
        self.data = {
            "CODE": kwargs.get("code"),
            "LABEL": kwargs.get("label"),
            "ACTIVE": kwargs.get("active"),
        }
        self.conflict_columns = (CoalesceableColumn("CODE", "''"),)
        self.updatable_columns = (CoalesceableColumn("LABEL", "''"),)


class Demand(PSQLTable):
    """This table should be ignored, it is populated from the application"""


class DemandStatut(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "DEMAND_STATUT"
        self.data = {
            "CODE": kwargs.get("code"),
            "LABEL": kwargs.get("label"),
        }
        self.conflict_columns = (CoalesceableColumn("CODE", None),)
        self.updatable_columns = (CoalesceableColumn("LABEL", "''"),)


class MailingList(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "MAILING_LIST"
        self.data = {
            "NAME": kwargs.get("name"),
            "SHARED": kwargs.get("shared"),
            "EXPORTED": kwargs.get("exported"),
            "UPDATE_DATE": datetime.now().timestamp(),
            "PERSON_ID": kwargs.get("person_id"),
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("NUMBER", None),
            CoalesceableColumn("PERSON_ID", 0),
        )
        self.updatable_columns = (
            CoalesceableColumn("NAME", None),
            CoalesceableColumn("SHARED", -1),
            CoalesceableColumn("EXPORTED", -1),
        )


class MailingListBalCcRecipients(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "MAILING_LIST_BAL_CC_RECIPIENTS"
        self.data = {
            "MAILING_LIST_ID": kwargs.get("mailing_list_id"),
            "BAL_CC_RECIPIENTS_ID": kwargs.get("bal_cc_recipients_id"),
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("MAILING_LIST_ID", None),
            CoalesceableColumn("BAL_CC_RECIPIENTS_ID", None),
        )
        self.updatable_columns = ()


class MailingListBalPrincipalRecipients(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "MAILING_LIST_BAL_PRINCIPAL_RECIPIENTS"
        self.data = {
            "MAILING_LIST_ID": kwargs.get("mailing_list_id"),
            "BAL_PRINCIPAL_RECIPIENTS_ID": kwargs.get("bal_principal_recipients_id"),
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("MAILING_LIST_ID", None),
            CoalesceableColumn("BAL_PRINCIPAL_RECIPIENTS_ID", None),
        )
        self.updatable_columns = ()


class MailingListCcRecipients(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "MAILING_LIST_CC_RECIPIENTS"
        self.data = {
            "MAILING_LIST_ID": kwargs.get("mailing_list_id"),
            "CC_RECIPIENTS_ID": kwargs.get("cc_recipients_id"),
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("MAILING_LIST_ID", None),
            CoalesceableColumn("CC_RECIPIENTS_ID", None),
        )
        self.updatable_columns = ()


class MailingListPrincipalRecipients(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "MAILING_LIST_PRINCIPAL_RECIPIENTS"
        self.data = {
            "MAILING_LIST_ID": kwargs.get("mailing_list_id"),
            "PRINCIPAL_RECIPIENTS_ID": kwargs.get("principal_recipients_id"),
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("MAILING_LIST_ID", None),
            CoalesceableColumn("PRINCIPAL_RECIPIENTS_ID", None),
        )
        self.updatable_columns = ()


class NeobaseChangelog(PSQLTable):
    """TODO"""


class NeobaseChangelogLock(PSQLTable):
    """TODO"""


class NeobaseSchema(PSQLTable):
    """TODO"""


class NeoUser(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "NEO_USER"
        self.data = {
            "USERNAME": kwargs.get("username"),
            "PASSWORD": kwargs.get("password"),
            "ACTIVE": kwargs.get("active", 1),
            "PASSWORD_CHANGE": kwargs.get("password_change", 0),
            "PROFILE_ID": kwargs.get("profile_id"),
            "DATA_SUPPLY_ID": kwargs.get("data_supply_id"),
            "PERSON_USER_ID": kwargs.get("person_user_id"),
        }
        self.max_value_lengths = {
            "USERNAME": 128,
            "PASSWORD": 128,
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("USERNAME", None),
            CoalesceableColumn("PERSON_USER_ID", 0)
        )
        self.updatable_columns = (
            CoalesceableColumn("PASSWORD", "''"),
        )

    def prepare_for_insert(self, data_supply_id, pg_connector):
        """Resolve foreign keys and perform other cleanups before insertion"""

        self.data["DATA_SUPPLY_ID"] = data_supply_id

        # Resolve foreign key person => PERSON_USER_ID
        if self.data.get("person"):
            self.data["PERSON_USER_ID"] = pg_connector.get_id_from(
                "PERSON",
                data_supply_id,
                keyval={
                    "IDENTITY_NUMBER": self.data["person"],
                    "DATA_SUPPLY_ID": data_supply_id,
                },
            )
            del self.data["person"]


class Person(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "PERSON"
        self.data = {
            "IDENTITY_NUMBER": kwargs.get("identity_number"),
            "ASP_ID": kwargs.get("asp_id"),
            "FIRST_NAME": kwargs.get("first_name"),
            "LAST_NAME": kwargs.get("last_name"),
            "FULL_NAME": kwargs.get("full_name"),
            "FUNCTION": kwargs.get("function"),
            "MANAGER_ID": kwargs.get("manager_id"),
            "STRUCTURE_ID": kwargs.get("structure_id"),
            "MAIL": kwargs.get("mail"),
            "UPDATE_DATE": datetime.now().timestamp(),
            "DATA_SUPPLY_ID": kwargs.get("data_supply_id"),
        }
        self.max_value_lengths = {
            "IDENTITY_NUMBER": 1024,
            "ASP_ID": 128,
            "FIRST_NAME": 128,
            "LAST_NAME": 128,
            "FULL_NAME": 256,
            "FUNCTION": 128,
            "MAIL": 1024,
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("IDENTITY_NUMBER", None),
            CoalesceableColumn("DATA_SUPPLY_ID", 0),
        )
        self.updatable_columns = (
            CoalesceableColumn("FIRST_NAME", None),
            CoalesceableColumn("LAST_NAME", None),
            CoalesceableColumn("FULL_NAME", "''"),
            CoalesceableColumn("FUNCTION", "''"),
            CoalesceableColumn("MANAGER_ID", 0),
            CoalesceableColumn("STRUCTURE_ID", 0),
            CoalesceableColumn("MAIL", "''"),
        )

    def prepare_for_insert(self, data_supply_id, pg_connector):
        """Resolve foreign keys and perform other cleanups before insertion"""

        self.data["DATA_SUPPLY_ID"] = data_supply_id

        # Resolve foreign key structure => STRUCTURE_ID
        if self.data.get("structure"):
            self.data["STRUCTURE_ID"] = pg_connector.get_id_from(
                "STRUCTURE",
                data_supply_id,
                keyval={"ASP_ID": None, "FULL_CODE": self.data.get("structure")},
            )
            del self.data["structure"]

        # Resolve foreign key manager => MANAGER_ID
        if self.data.get("manager"):
            self.data["MANAGER_ID"] = pg_connector.get_id_from(
                "PERSON",
                data_supply_id,
                keyval={
                    "IDENTITY_NUMBER": self.data.get("manager"),
                    "DATA_SUPPLY_ID": data_supply_id,
                },
            )
            del self.data["manager"]


class PersonFields(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "PERSON_FIELDS"
        self.data = {
            "LABEL": kwargs.get("label"),
            "CODE": kwargs.get("code"),
            "FIELD": kwargs.get("field"),
        }
        self.conflict_columns = (CoalesceableColumn("CODE", None),)
        self.updatable_columns = (CoalesceableColumn("LABEL", "''"),)


class Phone(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "PHONE"
        self.data = {
            "NUMBER": kwargs.get("number"),
            "PHONE_CATEGORY_ID": kwargs.get("phone_category_id"),
            "STRUCTURE_ID": kwargs.get("structure_id"),
            "PERSON_ID": kwargs.get("person_id"),
            "UPDATE_DATE": datetime.now().timestamp(),
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("NUMBER", None),
            CoalesceableColumn("PHONE_CATEGORY_ID", 0),
            CoalesceableColumn("STRUCTURE_ID", 0),
            CoalesceableColumn("PERSON_ID", 0),
        )
        self.updatable_columns = (
            CoalesceableColumn("NUMBER", None),
        )

    def prepare_for_insert(self, data_supply_id, pg_connector):
        """Resolve foreign keys and perform other cleanups before insertion"""

        # Resolve foreign key person => PERSON_ID
        if self.data.get("person"):
            self.data["PERSON_ID"] = pg_connector.get_id_from(
                "PERSON",
                data_supply_id,
                keyval={
                    "IDENTITY_NUMBER": self.data["person"],
                    "DATA_SUPPLY_ID": data_supply_id,
                },
            )
            del self.data["person"]

        # Resolve foreign key structure => STRUCTURE_ID
        if self.data.get("structure"):
            self.data["STRUCTURE_ID"] = pg_connector.get_id_from(
                "STRUCTURE",
                data_supply_id,
                keyval={"ASP_ID": None, "FULL_CODE": self.data["structure"]},
            )
            del self.data["structure"]


class PhoneCategory(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "PHONE_CATEGORY"
        self.data = {
            "CODE": kwargs.get("code"),
            "LABEL": kwargs.get("label"),
            "ACTIVE": kwargs.get("active"),
        }
        self.conflict_columns = (CoalesceableColumn("CODE", None),)
        self.updatable_columns = (CoalesceableColumn("LABEL", None),)


class Population(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "POPULATION"
        self.data = {
            "CODE": kwargs.get("code"),
            "LABEL": kwargs.get("label"),
            "ACTIVE": kwargs.get("active"),
        }
        self.conflict_columns = (CoalesceableColumn("CODE", None),)
        self.updatable_columns = (CoalesceableColumn("LABEL", "''"),)


class PostalAddress(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "POSTAL_ADDRESS"
        self.data = {
            "STREET": kwargs.get("street"),
            "ZIP_CODE": kwargs.get("zip_code"),
            "TOWN": kwargs.get("town"),
            "COUNTRY": kwargs.get("country"),
            "STRUCTURE_POSTAL_ADDRESS_ID": kwargs.get("structure_postal_address_id"),
            "PERSON_POSTAL_ADDRESS_ID": kwargs.get("person_postal_address_id"),
            "UPDATE_DATE": datetime.now().timestamp(),
        }
        self.max_value_lengths = {
            "STREET": 128,
            "ZIP_CODE": 128,
            "TOWN": 128,
            "COUNTRY": 128,
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("STREET", "''"),
            CoalesceableColumn("ZIP_CODE", "''"),
            CoalesceableColumn("TOWN", "''"),
            CoalesceableColumn("COUNTRY", "''"),
            CoalesceableColumn("STRUCTURE_POSTAL_ADDRESS_ID", 0),
            CoalesceableColumn("PERSON_POSTAL_ADDRESS_ID", 0),
        )
        self.updatable_columns = (
            CoalesceableColumn("STREET", "''"),
            CoalesceableColumn("ZIP_CODE", "''"),
            CoalesceableColumn("TOWN", "''"),
            CoalesceableColumn("COUNTRY", "''"),
        )

    def prepare_for_insert(self, data_supply_id, pg_connector):
        """Resolve foreign keys and perform other cleanups before insertion"""

        # Resolve foreign key person => PERSON_POSTAL_ADDRESS_ID
        if self.data.get("person"):
            self.data["PERSON_POSTAL_ADDRESS_ID"] = pg_connector.get_id_from(
                "PERSON",
                data_supply_id,
                keyval={
                    "IDENTITY_NUMBER": self.data["person"],
                    "DATA_SUPPLY_ID": data_supply_id,
                },
            )
            del self.data["person"]

        # Resolve foreign key structure => STRUCTURE_POSTAL_ADDRESS_ID
        if self.data.get("structure"):
            self.data["STRUCTURE_POSTAL_ADDRESS_ID"] = pg_connector.get_id_from(
                "STRUCTURE",
                data_supply_id,
                keyval={
                    "ASP_ID": None,
                    "FULL_CODE": self.data["structure"],
                },
            )
            del self.data["structure"]


class Profile(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "PROFILE"
        self.data = {
            "CODE": kwargs.get("code"),
            "LABEL": kwargs.get("label"),
            "ACTIVE": kwargs.get("active"),
        }
        self.conflict_columns = (CoalesceableColumn("CODE", None),)
        self.updatable_columns = (CoalesceableColumn("LABEL", "''"),)


class Rating(PSQLTable):
    """This table should be ignored, it is populated from the application"""

    # TODO: verify if this is true


class SharedMailbox(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "SHARED_MAILBOX"
        self.data = {
            "NAME": kwargs.get("name"),
            "DESCRIPTION": kwargs.get("description"),
            "MAIL": kwargs.get("mail"),
            "STRUCTURE_ID": kwargs.get("structure_id"),
            "UPDATE_DATE": datetime.now().timestamp(),
            "DATA_SUPPLY_ID": kwargs.get("data_supply_id"),
        }
        self.max_value_lengths = {
            "NAME": 128,
            "DESCRIPTION": 128,
            "MAIL": 1024,
        }
        self.dn = dn
        self.conflict_columns = (CoalesceableColumn("MAIL", None),)
        self.updatable_columns = (
            CoalesceableColumn("NAME", "''"),
            CoalesceableColumn("DESCRIPTION", "''"),
            CoalesceableColumn("STRUCTURE_ID", 0),
        )

    def prepare_for_insert(self, data_supply_id, pg_connector):
        """Resolve foreign keys and perform other cleanups before insertion"""

        self.data["DATA_SUPPLY_ID"] = data_supply_id

        # Resolve foreign key structure => STRUCTURE_ID
        if self.data.get("structure"):
            self.data["STRUCTURE_ID"] = pg_connector.get_id_from(
                "STRUCTURE",
                data_supply_id,
                keyval={"ASP_ID": None, "FULL_CODE": self.data.get("structure")},
            )
            del self.data["structure"]


class Structure(PSQLTable):
    def __init__(self, dn, **kwargs):
        super().__init__()
        self.tablename = "STRUCTURE"
        self.data = {
            "ASP_ID": kwargs.get("asp_id"),
            "FULL_CODE": kwargs.get("full_code"),
            "CODE": kwargs.get("code"),
            "LABEL": kwargs.get("label"),
            "PARENT_ID": kwargs.get("parent_id"),
            "MAIL": kwargs.get("mail"),
            "WEB_PORTAL": kwargs.get("web_portal"),
            "DATA_SUPPLY_ID": kwargs.get("data_supply_id"),
            "ASP_SERVICE_ID": kwargs.get("asp_service_id"),
            "UPDATE_DATE": datetime.now().timestamp(),
        }
        self.max_value_lengths = {
            "ASP_ID": 128,
            "FULL_CODE": 256,
            "CODE": 256,
            "LABEL": 256,
            "MAIL": 1024,
            "WEB_PORTAL": 256,
        }
        self.dn = dn
        self.conflict_columns = (
            CoalesceableColumn("ASP_ID", "''"),
            CoalesceableColumn("FULL_CODE", "''"),
        )
        self.updatable_columns = (
            CoalesceableColumn("CODE", "''"),
            CoalesceableColumn("LABEL", "''"),
            CoalesceableColumn("PARENT_ID", 0),
            CoalesceableColumn("MAIL", "''"),
            CoalesceableColumn("WEB_PORTAL", "''"),
        )

    def prepare_for_insert(self, data_supply_id, pg_connector):
        """Resolve foreign keys and perform other cleanups before insertion"""

        self.data["DATA_SUPPLY_ID"] = data_supply_id

        # Resolve foreign key parent => PARENT_ID
        if self.data.get("parent"):
            self.data["PARENT_ID"] = pg_connector.get_id_from(
                "STRUCTURE",
                data_supply_id,
                keyval={"ASP_ID": None, "FULL_CODE": self.data.get("parent")},
            )
            del self.data["parent"]

        # Resolve foreign key parent_asp_id => PARENT_ID
        if self.data.get("parent_asp_id"):
            self.data["PARENT_ID"] = pg_connector.get_id_from(
                "STRUCTURE",
                data_supply_id,
                keyval={"ASP_ID": self.data.get("parent_asp_id"), "FULL_CODE": None},
            )
            del self.data["parent_asp_id"]


class StructureFields(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "STRUCTURE_FIELDS"
        self.data = {
            "LABEL": kwargs.get("label"),
            "CODE": kwargs.get("code"),
            "FIELD": kwargs.get("field"),
        }
        self.conflict_columns = (CoalesceableColumn(name="CODE", coalesce=None),)
        self.updatable_columns = (CoalesceableColumn(name="LABEL", coalesce="''"),)


class SurveyAnswer(PSQLTable):
    """This table should be ignored, it is populated from the application"""


class SurveyQuestion(PSQLTable):
    def __init__(self, **kwargs):
        super().__init__()
        self.tablename = "SURVEY_QUESTION"
        self.data = {
            "CODE": kwargs.get("code"),
            "QUESTION": kwargs.get("question"),
            "ACTIVE": kwargs.get("active"),
        }
        self.conflict_columns = (CoalesceableColumn("CODE", None),)
        self.updatable_columns = (CoalesceableColumn("QUESTION", None),)
