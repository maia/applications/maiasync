"""Custom utils to handle CSV"""

import logging
import io
import csv
import base64
import warnings

from lib.ldifutils import (
    YieldingLdifParser,
    InputFileNeedsCorrection,
    canonicalize_entry,
)

log = logging.getLogger()


def parse_csv(filename, provider, batch_size=None):
    """Parse a CSV file, and for each row, create a LDIF and yield parsed dicts"""

    entry_readcount = 0
    entry_failcount = 0

    with open(filename, "r", encoding="utf-8-sig") as csv_file:
        reader = csv.DictReader(csv_file, delimiter=";")
        for row in reader:
            # Fix some common parsing issues
            # Additional delimiters in fields cause too many columns
            if len(row) != len(reader.fieldnames):
                warnings.warn(
                    f"Ignoring invalid CSV data (line #{reader.line_num} in file "
                    f"{filename}) because number of row columns doesn't match number "
                    f"of fields: {row}",
                    CSVNeedsCorrection,
                )
                continue
            # Structures without a CODE
            if row.get("parent_ou") and not row.get("code"):
                warnings.warn(
                    f"Ignoring invalid CSV data (line #{reader.line_num} in file "
                    f"{filename}) because it does not contain data for 'code': {row}",
                    CSVNeedsCorrection,
                )
                continue

            # Phone numbers are often badly encoded
            for phone_field in ("telephoneNumber", "mobile", "facsimileTelephoneNumber"):
                phone = row.get(phone_field)
                if phone:
                    phone = phone.replace("\xa0", " ")
                    row[phone_field] = phone
                    if phone.strip() == "Pas d'équipement":
                        del row[phone_field]
                        continue
                    if not phone.isascii() or "@" in phone:
                        warnings.warn(
                            f"Ignoring invalid data (line #{reader.line_num} in file "
                            f"{filename}). {phone_field}={phone}",
                            CSVNeedsCorrection,
                        )
                        del row[phone_field]

            # Department / full_code / code
            for service_field in ("Department", "full_code", "code", "parent_ou"):
                dpt_orig = row.get(service_field)
                if dpt_orig:
                    if "\\" in dpt_orig:
                        dpt_fixed = dpt_orig.replace("\\", "/")
                        warnings.warn(
                            f"Field '{service_field}' auto-corrected (line #{reader.line_num} "
                            f"in file {filename}): '{dpt_orig}' => '{dpt_fixed}'",
                            CSVNeedsCorrection,
                        )
                        row[service_field] = dpt_fixed

            raw_ldif = csv_to_ldif(row, provider)
            fake_ldif_file = io.StringIO(raw_ldif)
            try:
                entry_readcount += 1
                yielding_parser = YieldingLdifParser(fake_ldif_file)
                yielding_parser.parse()
                for dn, entry in yielding_parser.entries:
                    yield canonicalize_entry(dn, entry)
                if batch_size and entry_readcount % batch_size == 0:
                    log.info("%s entries read so far...", entry_readcount)
            except ValueError as e:
                entry_failcount += 1
                error_msg = f"Failed to parse ldif entry from CSV, Reason: {e}"
                dn_for_error_msg = raw_ldif.split("\n")[0]
                error_msg += f"\n  {dn_for_error_msg}"
                warnings.warn(error_msg, CSVNeedsCorrection)
            except Exception:
                log.critical("Unexpected error.", exc_info=True)
                break


def csv_to_ldif(row, provider):
    """Given a row, convert the fields to LDAP attributes"""

    row = {k.lower(): v for k, v in row.items()}

    # Route to person, service or shared_mailbox
    if "givenname" in row and "surname" in row:
        ldif_dict = _csv_to_ldif_person(row, provider)
    elif "full_code" in row and "parent_ou" in row and "code" in row:
        ldif_dict = _csv_to_ldif_service(row, provider)
    else:
        raise NotImplementedError('Unable to guess data type based on these CSV columns: "{}"'.format('", "'.join(row.keys())))

    # Generate LDIF entry from the returned dict
    ldif = f"dn: {ldif_dict.pop('dn')}\n"
    for attr, value in ldif_dict.items():
        if not value:
            continue
        if isinstance(value, str):
            ldif += f"{attr}: {value}\n"
        elif isinstance(value, bytes):  # if we get bytes it's a b64 encoded value
            ldif += f"{attr}:: {value.decode()}\n"
        elif isinstance(value, tuple):  # multiple identical attributes
            ldif += "\n".join(f"{attr}: {val}" for val in value) + "\n"
        else:
            # Safeguard
            raise NotImplementedError(f"Unknown value for {attr}: {value}")

    return ldif


def _csv_to_ldif_person(row, provider):
    """CSV to LDIF for a Person object"""

    ldif = {
        "dn": f"mail={row['emailaddress']},ou=personnes,ou={provider},ou=gouv,dc=service-public,dc=fr",
        "givenName": _str_or_b64(row["givenname"]),
        "sn": row["surname"],
        "cn": _str_or_b64(f"{row['surname']} {row['givenname']}"),
        "displayName": _str_or_b64(f"{row['surname']} {row['givenname']}"),
        "mail": row["emailaddress"],
        "objectClass": (
            "frGovPerson",
            "euGovPerson",
            "inetOrgPerson",
            "organizationalPerson",
            "top",
        ),
        "ou": f"{provider}/{row['department']}",
        "departmentNumber": row["department"],
    }

    # Nullable values:
    if "title" in row and row["title"]:
        ldif["title"] = row["title"]
    if "telephonenumber" in row and row["telephonenumber"]:
        ldif["telephoneNumber"] = row["telephonenumber"]
    if "mobile" in row and row["mobile"]:
        ldif["mobile"] = row["mobile"]
    if "streetaddress" in row and row["streetaddress"]:
        ldif["street"] =  _str_or_b64(row["streetaddress"])
    if "city" in row and row["city"]:
        ldif["l"] = row["city"]
    if "postalcode" in row and row["postalcode"]:
        ldif["postalCode"] = row["postalcode"]
    if "state" in row and row["state"]:
        ldif["c"] = row["state"]
    if "manager" in row and row["manager"]:
        manager = row["manager"]
        if not manager.startswith("mail="):
            manager = "mail=" + manager
        ldif["manager"] = f"{manager},ou=personnes,ou={provider},ou=gouv,dc=service-public,dc=fr"

    return ldif


def _csv_to_ldif_service(row, provider):
    """CSV to LDIF for a Service object"""

    ldif = {
        "dn": f"departmentUID={row['full_code']},ou=services,ou={provider},ou=gouv,dc=service-public,dc=fr",
        "objectClass": (
            "frGovOrganizationalUnit",
            "euGovOrganizationalUnit",
            "organizationalUnit",
            "top",
        ),
        "ou": row["code"],
        "parentOu": row["parent_ou"],
        "departmentNumber": row["full_code"],
        "departmentUID": row["full_code"],
    }

    return ldif


def _str_or_b64(value):
    """Base64 encode the value if unprintable characters are found"""

    for char in value:
        if ord(char) < 32 or ord(char) > 127:
            return base64.b64encode(value.encode())
    return value


class CSVNeedsCorrection(InputFileNeedsCorrection):
    """Warning for when a given CSV file needs correcting."""

    def __init__(self, message):
        self.message = message
        super().__init__(message)
