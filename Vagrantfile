# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.define "maia-pgsql-local" do |maia_pgsql|
    maia_pgsql.vm.box = "debian/bullseye64"
    maia_pgsql.vm.hostname = "maia-pgsql-local"
    maia_pgsql.vm.network "private_network", ip: "192.168.121.147"
    maia_pgsql.vm.provider :libvirt do |libvirt|
      libvirt.memory = 2048
      libvirt.cpus = 2
    end

    maia_pgsql.vm.provision "shell" do |shell|
      shell.inline = <<-SHELL
        apt-get update
        apt-get install sudo vim postgresql -y
        sudo -iu postgres createuser neouser
        sudo -iu postgres psql -c "ALTER USER neouser PASSWORD 'foo';"
        sudo -iu postgres dropdb neobasemaia
        sudo -iu postgres createdb -O neouser neobasemaia
        echo "host   neobasemaia   neouser   0.0.0.0/0   md5" >> /etc/postgresql/13/main/pg_hba.conf
        echo "listen_addresses = '*'" > /etc/postgresql/13/main/conf.d/listen.conf
        systemctl restart postgresql
        sudo -iu postgres psql -d neobasemaia < /vagrant/sql/neobasemaia_schema.sql
        sudo -iu postgres psql -d neobasemaia < /vagrant/sql/ee_schema_additions.sql
      SHELL
    end
  end

  config.vm.define "maia-openldap-local" do |maia_openldap|
    maia_openldap.vm.box = "debian/bullseye64"
    maia_openldap.vm.hostname = "maia-openldap-local"
    maia_openldap.vm.network "private_network", ip: "192.168.121.10"
    maia_openldap.vm.provider :libvirt do |libvirt|
      libvirt.memory = 2048
      libvirt.cpus = 2
    end

    maia_openldap.vm.provision "shell" do |shell|
      shell.inline = <<-SHELL
        apt-get update
        export DEBIAN_FRONTEND=noninteractive
        apt-get -o Dpkg::Options::="--force-confold" -o Dpkg::Options::="--force-confdef" install -y sudo vim slapd ldapvi ldap-utils lmdb-utils
        systemctl stop slapd
        find /var/lib/ldap -type f -delete
        rm -f /etc/ldap/slapd.d/cn=config/olcDatabase={1}mdb.ldif
        systemctl start slapd
        sleep 4
        echo -e "dn: cn=config\nchangeType: modify\nreplace: olcLogLevel\nolcLogLevel: stats\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=config 2>/dev/null
        echo -e "dn: cn=module{0},cn=config\nchangeType: modify\nadd: olcModuleLoad\nolcModuleLoad: memberof\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=module{0},cn=config 2>/dev/null
        echo -e "dn: cn=module{0},cn=config\nchangeType: modify\nadd: olcModuleLoad\nolcModuleLoad: dynlist\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=module{0},cn=config 2>/dev/null
        echo -e "dn: cn=module{0},cn=config\nchangeType: modify\nadd: olcModuleLoad\nolcModuleLoad: pw-sha2\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=module{0},cn=config 2>/dev/null
        ldapadd -Y EXTERNAL -H ldapi:/// -f /vagrant/ldif_setup/maia_schema.ldif 2>/dev/null
        ldapadd -Y EXTERNAL -H ldapi:/// -f /vagrant/ldif_setup/maia_database.ldif 2>/dev/null
        export ADMINPW=$(slappasswd -o module-load=pw-sha2 -h "{SSHA512}" -s foo)
        echo -e "dn: olcDatabase={0}config,cn=config\nchangeType: modify\nadd: olcRootPW\nolcRootPW: $ADMINPW\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=config 2>/dev/null
        export APPPW=$(slappasswd -o module-load=pw-sha2 -h "{SSHA512}" -s foo)
        echo -e "dn: olcDatabase={1}mdb,cn=config\nchangeType: modify\nadd: olcRootPW\nolcRootPW: $APPPW\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=config 2>/dev/null
      SHELL
    end
  end
end
