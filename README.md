# maiasync

## Requirements

```bash
apt install python3-psycopg2 python3-ldap
cp config.example.ini config.ini  # and modify accordingly
```

## Versioning

maiasync uses [semantic versioning](https://semver.org/):

    Given a version number MAJOR.MINOR.PATCH, increment the:

        MAJOR version when you make incompatible API changes
        MINOR version when you add functionality in a backward compatible manner
        PATCH version when you make backward compatible bug fixes

This tries to prevent breaking changes to be deployed with `git pull`: always checkout to a specific tag when updating.

## Usage

```bash
./maiasync -h
```

By default, maiasync loads to PSQL.
The provider, if omitted, is computed from existing input files.

## Updating the database schema

To use the `UPSERT` capacities of PostgreSQL, we need additional `UNIQUE` constraints on the existing schema.
Therefore, we need to update the existing schema with our additions.

The `update_psql_schema` script takes a schema dump as argument.

```bash
./update_psql_schema ./sql/ee_schema_additions.sql
```

It also removes duplicate data in the database before applying the update.

## Vagrant quickstart (for testing on a local VM)

```bash
apt install vagrant libvirt-dev nfs-kernel-server
vagrant plugin install vagrant-libvirt
vagrant up
```

## Testing on some data

The folder `.sample_data` is gitignored.
Put any `ldif` file you want in there, and feed the path to that file to `maiasync`.
