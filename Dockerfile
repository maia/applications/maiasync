FROM debian:bullseye
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get upgrade --no-install-recommends -y && \
    apt-get install --no-install-recommends -y \
        sudo \
        vim \
        postgresql \
        slapd \
        ldapvi \
        ldap-utils \
        lmdb-utils

# PSQL
COPY sql/neobasemaia_schema.sql /tmp/
COPY sql/ee_schema_additions.sql /tmp/
RUN /etc/init.d/postgresql start && \
    sudo -iu postgres createuser neouser && \
    sudo -iu postgres psql -c "ALTER USER neouser PASSWORD 'foo';" && \
    sudo -iu postgres createdb -O neouser neobasemaia && \
    echo "host   neobasemaia   neouser   0.0.0.0/0   md5" >> /etc/postgresql/13/main/pg_hba.conf && \
    echo "listen_addresses = '*'" > /etc/postgresql/13/main/conf.d/listen.conf && \
    pg_ctlcluster 13 main restart && \
    sudo -iu postgres psql -d neobasemaia < /tmp/neobasemaia_schema.sql && \
    sudo -iu postgres psql -d neobasemaia < /tmp/ee_schema_additions.sql
EXPOSE 5432

# LDAP
COPY ldif_setup/maia_schema.ldif /tmp/
COPY ldif_setup/maia_database.ldif /tmp/
RUN find /var/lib/ldap -type f -delete && \
    rm -f /etc/ldap/slapd.d/cn=config/olcDatabase={1}mdb.ldif && \
    /etc/init.d/slapd start && \
    sleep 4 && \
    echo "dn: cn=config\nchangeType: modify\nreplace: olcLogLevel\nolcLogLevel: stats\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=config && \
    echo "dn: cn=module{0},cn=config\nchangeType: modify\nadd: olcModuleLoad\nolcModuleLoad: memberof\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=module{0},cn=config && \
    echo "dn: cn=module{0},cn=config\nchangeType: modify\nadd: olcModuleLoad\nolcModuleLoad: dynlist\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=module{0},cn=config && \
    echo "dn: cn=module{0},cn=config\nchangeType: modify\nadd: olcModuleLoad\nolcModuleLoad: pw-sha2\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=module{0},cn=config && \
    ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/maia_schema.ldif && \
    ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/maia_database.ldif && \
    export ADMINPW=$(slappasswd -s foo) && \
    echo "dn: olcDatabase={0}config,cn=config\nchangeType: modify\nadd: olcRootPW\nolcRootPW: ${ADMINPW}\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=config && \
    export APPPW=$(slappasswd -s foo) && \
    echo "dn: olcDatabase={1}mdb,cn=config\nchangeType: modify\nadd: olcRootPW\nolcRootPW: ${APPPW}\n" | ldapmodify -Y EXTERNAL -H ldapi:/// -D cn=config
EXPOSE 389

STOPSIGNAL SIGKILL

CMD /etc/init.d/postgresql start && \
    /etc/init.d/slapd start && \
    tail -f /var/log/postgresql/postgresql-13-main.log
