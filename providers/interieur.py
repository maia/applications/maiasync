"""Entry point for interieur.ldif (MIOM) """

import warnings
import logging
from lib.ldifutils import parse_ldif, LdifToPsqlTableConverter, MaiaLDIFCleaner
from lib.ldifutils import LDIFNeedsCorrection

log = logging.getLogger()

prefix = ""
suffix = ""

criteria = {
    "is_person": (b"fimadFrGovPerson",),
    "is_shared_mailbox": (b"fimadFrGovPerson",),
    "is_structure": (b"frGovOrganizationalUnit",),
    "data_supply": "MIOM",
    "bad_suffixes": ["o=GOUV,c=FR"],
    "person": {
        "IDENTITY_NUMBER": "mail",
        "FIRST_NAME": "givenname",
        "LAST_NAME": "sn",
        "FULL_NAME": "cn",
        "FUNCTION": "title",
        "MAIL": "mail",
        "structure": "departmentnumber",
        "manager": "manager",
        "mailboxtype": "mailboxtype",
    },
    "shared_mailbox": {
        "NAME": "cn",
        "DESCRIPTION": "description",
        "MAIL": "mail",
        "FUNCTION": "title",
        "structure": "departmentnumber",
        "mailboxtype": "mailboxtype",
    },
    "structure": {
        "FULL_CODE": "departmentnumber",
        "CODE": "ou",
        "LABEL": "description",
        "MAIL": "mail",
        "WEB_PORTAL": "labeleduri",
        "parent": "parentou",
    },
}


def iter_entries(inputfiles, **kwargs):
    """Yield objects suitable for DB or LDAP insertion.

    Each object will either be a dbitem for DB insertion or a (dn, entry) tuple for
    LDAP insertion.
    """

    ldif_converter = LdifToPsqlTableConverter(criteria)
    ldif_cleaner = MaiaLDIFCleaner(criteria)

    # We need to remember person dbitems that we decide to ignore in order to also ignore any
    # derivated dbitems associated (phones, addresses, neouser, ...)
    person_ignored = set()

    for inputfile in inputfiles:
        for dn, entry in parse_ldif(inputfile):
            for dbitem in ldif_converter.iter_items(dn, entry):
                if dbitem.tablename == "PERSON":
                    if "mailboxtype" not in dbitem.data:
                        value = dbitem.data.get("FUNCTION")
                        if value:
                            if len(dbitem.data["FUNCTION"]) > 10:
                                if dbitem.data["FUNCTION"][0:12] == "BAL FONCTION":
                                    person_ignored.add(dbitem.data.get("IDENTITY_NUMBER"))
                                    continue
                    else:
                        if dbitem.data["mailboxtype"].startswith("Fonctionnelle"):
                            person_ignored.add(dbitem.data.get("IDENTITY_NUMBER"))
                            continue
                        del dbitem.data["mailboxtype"]

                if dbitem.tablename == "STRUCTURE" and dbitem.data.get("parent") == "/FR/GOUV":
                    continue

                if dbitem.tablename == "SHARED_MAILBOX":
                    if "mailboxtype" not in dbitem.data:
                        value = dbitem.data.get("FUNCTION")
                        if value:
                            if len(dbitem.data["FUNCTION"]) > 10:
                                if dbitem.data["FUNCTION"][0:12] != "BAL FONCTION":
                                    continue
                            else:
                                continue
                    else:
                        if dbitem.data["mailboxtype"].startswith("Personnelle"):
                            continue
                        del dbitem.data["mailboxtype"]
                    if dbitem.data.get("FUNCTION"):
                        del dbitem.data["FUNCTION"]

                if dbitem.tablename in ("PHONE", "NEO_USER", "POSTAL_ADDRESS"):
                    if dbitem.data.get("person") in person_ignored:
                        continue

                if dbitem.data.get("structure"):
                    dbitem.data["structure"] = dbitem.data["structure"].removeprefix("/FR/GOUV/")
                if dbitem.data.get("parent"):
                    dbitem.data["parent"] = dbitem.data["parent"].removeprefix("/FR/GOUV/")
                if dbitem.data.get("FULL_CODE"):
                    dbitem.data["FULL_CODE"] = dbitem.data["FULL_CODE"].removeprefix("/FR/GOUV/")

                yield dbitem

            for dn, entry, ignore_attr_types in ldif_cleaner.iter_items(dn, entry):
                yield dn, entry, ignore_attr_types
