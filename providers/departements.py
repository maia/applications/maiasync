"""Entry point for departements.ldif"""

import logging
from lib.ldifutils import parse_ldif, LdifToPsqlTableConverter, MaiaLDIFCleaner

log = logging.getLogger()

criteria = {
    "is_person": (b"fimadFrGovPerson",),
    "is_shared_mailbox": (b"fimadFrGovOrganizationalRole",),
    "is_structure": (b"frGovOrganizationalUnit", b"organizationalUnit"),
    "data_supply": "ALCD",
    "bad_suffixes": ["ou=ANNUAIRE FEDERE,o=gouv,c=fr"],
    "person": {
        "IDENTITY_NUMBER": "mail",
        "FIRST_NAME": "givenname",
        "LAST_NAME": "sn",
        "FULL_NAME": "cn",
        "FUNCTION": "title",
        "MAIL": "mail",
        "structure": "departmentnumber",
        "manager": "manager",
    },
    "shared_mailbox": {
        "NAME": "cn",
        "DESCRIPTION": "description",
        "MAIL": "mail",
        "structure": "departmentnumber",
    },
    "structure": {
        "FULL_CODE": "departmentnumber",
        "CODE": "ou",
        "LABEL": "description",
        "MAIL": "mail",
        "WEB_PORTAL": "labeleduri",
        "parent": "parentou",
    },
}


def iter_entries(inputfiles, **kwargs):
    """Yield objects suitable for DB or LDAP insertion.

    Each object will either be a dbitem for DB insertion or a (dn, entry) tuple for
    LDAP insertion.
    """

    ldif_converter = LdifToPsqlTableConverter(criteria)
    ldif_cleaner = MaiaLDIFCleaner(criteria)

    for inputfile in inputfiles:
        for dn, entry in parse_ldif(inputfile):
            for dbitem in ldif_converter.iter_items(dn, entry):
                if dbitem.tablename == "STRUCTURE" and dbitem.data.get("parent") in ("/FR/GOUV", "/FR/GOUV/ANNUAIRE FEDERE"):
                    log.debug("Ignoring STRUCTURE entry because parent is /FR/GOUV. dn=%s.", dn)
                    continue

                if dbitem.data.get("structure"):
                    dbitem.data["structure"] = "departements" + dbitem.data["structure"].removeprefix("/FR/GOUV/ANNUAIRE FEDERE/DEPARTEMENTS")
                if dbitem.data.get("parent"):
                    dbitem.data["parent"] = "departements" + dbitem.data["parent"].removeprefix("/FR/GOUV/ANNUAIRE FEDERE/DEPARTEMENTS")
                if dbitem.data.get("FULL_CODE"):
                    dbitem.data["FULL_CODE"] = "departements" + dbitem.data["FULL_CODE"].removeprefix("/FR/GOUV/ANNUAIRE FEDERE/DEPARTEMENTS")

                yield dbitem

            for dn, entry, ignore_attr_types in ldif_cleaner.iter_items(dn, entry):
                yield dn, entry, ignore_attr_types
