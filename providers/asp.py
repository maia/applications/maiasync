"""Entry point for asp*.json"""

from datetime import datetime
import json
import logging
from lib.jsonutils import iter_json_objects
import lib.psql as model

log = logging.getLogger()

criteria = {
    "data_supply": "ASP",
}


def _extract_from_json_obj(obj):
    """
    À partir d'une entrée JSON, construit des objets DbItem de type
    "PERSON", "STRUCTURE", "PHONE".
    """
    structure = model.Structure(
        None,
        data_supply_id=criteria["data_supply"]
    )
    structure_id = obj.get("id")
    structure.data = {
        "ASP_ID": structure_id,
        "FULL_CODE": None,
        "CODE": obj.get("nom"),
        "LABEL": None,
        "ASP_SERVICE_ID": None,
        "UPDATE_DATE": datetime.now().timestamp(),
    }

    if obj.get("adresse_courriel"):
        structure.data["MAIL"] = obj.get("adresse_courriel")

    if obj.get("site_internet"):
        tmp = obj.get("site_internet")
        if tmp.startswith("["):
            tmp = json.loads(tmp)[0].get("valeur")
        if len(tmp) > 255:
            log.warning("Ignoring Website URL because it's too long: id=%s, url=%s", obj.get("id"), tmp)
        else:
            structure.data["WEB_PORTAL"] = tmp

    # une entrée pour "STRUCTURE"
    yield structure

    # les entrées pour "PERSON"
    if obj.get("affectation_personne"):
        for affectation_personne in json.loads(obj.get("affectation_personne")):
            person_obj = affectation_personne.get("personne")
            if not person_obj:
                continue
            person_email_address = None
            person_fname = person_obj.get("prenom")
            person_lname = person_obj.get("nom")
            person_fullname = f"{person_fname} {person_lname}".strip()
            if person_obj.get("adresse_courriel"):
                mails = person_obj.get("adresse_courriel")
                person_email_address = mails[0].get("valeur")
                if len(mails) > 1:
                    log.warning("%s email addresses for person %s. Only this one will be kept: %s", len(mails), person_fullname, person_email_address)
            if not person_email_address:
                log.debug("No email address found for %s. Ignoring.", person_fullname)
                continue
            person = model.Person(None, data_supply_id=criteria["data_supply"],)
            person.data = {
                "IDENTITY_NUMBER": person_email_address,
                "MAIL": person_email_address,
                "ASP_ID": None, # TODO guess or compute ASP_ID for PERSON, if it makes sense
                "FIRST_NAME": person_fname,
                "LAST_NAME": person_lname,
                "FULL_NAME": person_fullname,
                "FUNCTION": affectation_personne.get("fonction"),
                "MANAGER_ID": None,
                "UPDATE_DATE": datetime.now().timestamp(),
            }
            yield person


def iter_entries(inputfiles, **kwargs):
    """Yield objects suitable for DB or LDAP insertion.

    Each object will either be a dbitem for DB insertion or a (dn, entry) tuple for
    LDAP insertion.
    """

    for inputfile in inputfiles:
        # Première passe, pour identifier les imbrications de structures
        map_structure_parent = {}
        for obj in iter_json_objects(inputfile):
            if obj.get("hierarchie"):
                hierarchie = json.loads(obj.get("hierarchie"))
                for child_service in hierarchie:
                    if child_service.get("type_hierarchie") == "Service Fils":
                        child_id = child_service.get("service")
                        map_structure_parent[child_id] = obj.get("id")

        # Seconde passe, pour enregistrer en BDD
        for obj in iter_json_objects(inputfile):
            for dbitem in _extract_from_json_obj(obj):
                if isinstance(dbitem, model.Structure):
                    itemid = dbitem.data.get("ASP_ID")
                    if itemid in map_structure_parent:
                        dbitem.data["parent_asp_id"] = map_structure_parent.get(itemid)
                yield dbitem

            # TODO: implement LDAP for ASP
            # for dn, entry, ignore_attr_types in ...
            #     yield dn, entry, ignore_attr_types
