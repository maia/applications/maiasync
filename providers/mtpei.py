"""Entry point for Communs_*.csv"""

import logging
import os
from lib.csvutils import parse_csv
from lib.ldifutils import LdifToPsqlTableConverter, MaiaLDIFCleaner

log = logging.getLogger()

criteria = {
    "is_person": (b"frGovPerson",),
    "is_shared_mailbox": (b"frGovOrganizationalRole",),
    "is_structure": (b"frGovOrganizationalUnit",),
    "data_supply": "MTPEI",
    "bad_suffixes": [],
    "person": {
        "IDENTITY_NUMBER": "mail",
        "FIRST_NAME": "givenname",
        "LAST_NAME": "sn",
        "FULL_NAME": "cn",
        "FUNCTION": "title",
        "MAIL": "mail",
        "manager": "manager",
        "structure": "departmentnumber",
        "street": "streetaddress",
    },
    "shared_mailbox": {
        "NAME": "cn",
        "DESCRIPTION": "description",
        "MAIL": "mail",
        "structure": "departmentnumber",
    },
    "structure": {
        "FULL_CODE": "departmentnumber",
        "CODE": "ou",
        "LABEL": "description",
        "MAIL": "mail",
        "WEB_PORTAL": "labeleduri",
        "parent": "parentou",
    },
}


def _guess_structure_shortname(filename):
    """Parse the filename to determine the prefix to apply"""

    filename_prefixes = {
        "communs": "mes-co",
        "travail": "mes-tr",
        "sante": "mes-sa",
        "solidarites": "mes-so",
    }

    for prefix, structure_shortname in filename_prefixes.items():
        if prefix in filename.lower():
            return structure_shortname
    return None


def iter_entries(inputfiles, **kwargs):
    """Yield objects suitable for DB or LDAP insertion.

    Each object will either be a dbitem for DB insertion or a (dn, entry) tuple for
    LDAP insertion.
    """

    ldif_converter = LdifToPsqlTableConverter(criteria)
    ldif_cleaner = MaiaLDIFCleaner(criteria)

    for inputfile in inputfiles:
        structure_shortname = _guess_structure_shortname(inputfile)
        for dn, entry in parse_csv(
            inputfile,
            provider=criteria["data_supply"],
        ):
            for dbitem in ldif_converter.iter_items(dn, entry):
                if dbitem.data.get("structure"):
                    dbitem.data["structure"] = f"{structure_shortname}/{dbitem.data['structure']}"
                yield dbitem

            for dn, entry, ignore_attr_types in ldif_cleaner.iter_items(dn, entry):
                yield dn, entry, ignore_attr_types
