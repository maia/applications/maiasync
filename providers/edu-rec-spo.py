"""Entry point for education-recherche-sports.ldif"""

import logging
from lib.ldifutils import parse_ldif, LdifToPsqlTableConverter, MaiaLDIFCleaner

log = logging.getLogger()

criteria = {
    "is_person": (b"frGovPerson",),
    "is_shared_mailbox": (b"frGovOrganizationalRole", b"organizationalRole"),
    "is_structure": (b"frGovOrganizationalUnit", b"organizationalUnit"),
    "data_supply": "MENJ",
    "bad_suffixes": [
        "ou=education,ou=gouv,dc=service-public,dc=fr",
        "ou=education,ou=gouv.fr,dc=service-public,dc=fr",
    ],
    "person": {
        "IDENTITY_NUMBER": "mail",
        "FIRST_NAME": "givenname",
        "LAST_NAME": "sn",
        "FULL_NAME": "cn",
        "FUNCTION": "title",
        "MAIL": "mail",
        "manager": "manager",
        "structure": "departmentnumber",
    },
    "shared_mailbox": {
        "NAME": "cn",
        "DESCRIPTION": "description",
        "MAIL": "mail",
        "structure": "departmentnumber",
    },
    "structure": {
        "FULL_CODE": "departmentnumber",
        "CODE": "ou",
        "LABEL": "description",
        "MAIL": "mail",
        "WEB_PORTAL": "labeleduri",
        "parent": "parentou",
    },
}

def iter_entries(inputfiles, **kwargs):
    """Yield objects suitable for DB or LDAP insertion.

    Each object will either be a dbitem for DB insertion or a (dn, entry) tuple for
    LDAP insertion.
    """

    ldif_converter = LdifToPsqlTableConverter(criteria)
    ldif_cleaner = MaiaLDIFCleaner(criteria)

    for inputfile in inputfiles:
        for dn, entry in parse_ldif(inputfile):
            for dbitem in ldif_converter.iter_items(dn, entry):
                yield dbitem

            for dn, entry, ignore_attr_types in ldif_cleaner.iter_items(dn, entry):
                yield dn, entry, ignore_attr_types
