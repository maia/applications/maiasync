"""Entry point for pm.ldif (PM) """

import logging
from lib.ldifutils import parse_ldif, LdifToPsqlTableConverter, MaiaLDIFCleaner

log = logging.getLogger()

prefix = ""
suffix = ""

criteria = {
    "is_person": (b"frGovPerson",),
    "is_shared_mailbox": (b"frGovOrganizationalRole",),
    "is_structure": (b"frGovOrganizationalUnit",),
    "data_supply": "PM",
    "bad_suffixes": ["ou=pm,ou=gouv,dc=service-public,dc=fr"],
    "person": {
        "IDENTITY_NUMBER": "mail",
        "FIRST_NAME": "givenname",
        "LAST_NAME": "sn",
        "FULL_NAME": "cn",
        "FUNCTION": "description",
        "MAIL": "mail",
        "structure": "departmentnumber",
        "manager": "manager",
    },
    "shared_mailbox": {
        "NAME": "cn",
        "DESCRIPTION": "description",
        "MAIL": "mail",
        "title": "title",
        "structure": "departmentnumber",
    },
    "structure": {
        "FULL_CODE": "departmentnumber",
        "CODE": "ou",
        "LABEL": "description",
        "MAIL": "mail",
        "WEB_PORTAL": "labeleduri",
        "parent": "parent_ou",
    },
}

def iter_entries(inputfiles, **kwargs):
    """Yield objects suitable for DB or LDAP insertion.

    Each object will either be a dbitem for DB insertion or a (dn, entry) tuple for
    LDAP insertion.
    """

    ldif_converter = LdifToPsqlTableConverter(criteria)
    ldif_cleaner = MaiaLDIFCleaner(criteria)

    for inputfile in inputfiles:
        for dn, entry in parse_ldif(inputfile):
            for dbitem in ldif_converter.iter_items(dn, entry):
                if dbitem.data.get("structure"):
                    dbitem.data["structure"] = dbitem.data["structure"].upper()
                if dbitem.data.get("parent"):
                    dbitem.data["parent"] = dbitem.data["parent"].upper()
                if dbitem.data.get("FULL_CODE"):
                    dbitem.data["FULL_CODE"] = dbitem.data["FULL_CODE"].upper()

                yield dbitem

            for dn, entry, ignore_attr_types in ldif_cleaner.iter_items(dn, entry):
                yield dn, entry, ignore_attr_types
